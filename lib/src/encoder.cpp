/**
 * @file encoder.cpp
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Июнь 2015 года
*/

#include "../lib/include/encoder.h"
#include "../lib/include/delay.h"
#include "stm32f4xx_gpio.h"

using namespace EncoderName;
using namespace DelayName;

Encoder::Encoder(Encoder_TypeDef type, float koef, float zeroAngle)
{
	this->zeroAngle = zeroAngle;
	angle = 0.f;
	speed = 0.f;
	this->koef = koef;
	counter = 0;
	time = Delay::GetMicrosecond();
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	TIM_TimeBaseInitTypeDef TIM_BaseInitStructure;
	TIM_BaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseInitStructure.TIM_CounterMode = TIM_CounterMode_Down | TIM_CounterMode_Up;
	TIM_BaseInitStructure.TIM_Prescaler = 0;
	TIM_BaseInitStructure.TIM_Period = 0xFFFF;
	TIM_BaseInitStructure.TIM_RepetitionCounter = 0;

	switch (type) {
		case Encoder_2:
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
			GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
			port = GPIOA;
			GPIO_PinAFConfig(port, GPIO_PinSource0, GPIO_AF_TIM2);
			GPIO_PinAFConfig(port, GPIO_PinSource1, GPIO_AF_TIM2);
			timer = TIM2;
			break;
		case Encoder_3:
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
			GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
			port = GPIOC;
			GPIO_PinAFConfig(port, GPIO_PinSource6, GPIO_AF_TIM3);
			GPIO_PinAFConfig(port, GPIO_PinSource7, GPIO_AF_TIM3);
			timer = TIM3;
			break;
		case Encoder_5:
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
			GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
			port = GPIOA;
			GPIO_PinAFConfig(port, GPIO_PinSource0, GPIO_AF_TIM5);
			GPIO_PinAFConfig(port, GPIO_PinSource1, GPIO_AF_TIM5);
			timer = TIM5;
			break;
		case Encoder_8:
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
			GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
			port = GPIOC;
			GPIO_PinAFConfig(port, GPIO_PinSource6, GPIO_AF_TIM8);
			GPIO_PinAFConfig(port, GPIO_PinSource7, GPIO_AF_TIM8);
			timer = TIM8;
			break;
		default:
			break;
	}

	GPIO_Init(port, &GPIO_InitStructure);
	TIM_TimeBaseInit(timer, &TIM_BaseInitStructure);

	TIM_EncoderInterfaceConfig(timer, TIM_EncoderMode_TI1,
			TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_Cmd(timer, ENABLE);
}

void Encoder::Update()
{
	//один оборот двигателя - 1 значение счетчика
	counter = TIM_GetCounter(timer);
	TIM_SetCounter(timer, 0x7FFF);
	speed = 1000000 * (counter - 0x7FFF) / (Delay::GetMicrosecond() - time);
	time = Delay::GetMicrosecond();
	angle = zeroAngle + (counter - 0x7FFF) * koef * 360.f;
	if(angle > 180.f)
		angle -= 360.f;
	else if(angle < -180.f)
		angle += 360.f;
}
