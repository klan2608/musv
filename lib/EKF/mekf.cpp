#include "mekf.h"
#include "matrixoperations.h"
#include "new_expm.h"
#include "consts.h"

FilterData::FilterData()
{
    memset( this, 0, sizeof( FilterData ) );
}

void FilterData::FilterThis()
{
	static FLOAT_TYPE AA[MATRIXSIZE * MATRIXSIZE] = {0}; // 32x32
	static FLOAT_TYPE* BB = nullptr; // 32x32
    static FLOAT_TYPE K[MATRIXSIZE / 2 * 9];
    
    //Temps
    static FLOAT_TYPE tempHalfMatrix1[MATRIXSIZE * MATRIXSIZE / 4]; // 16x16
    static FLOAT_TYPE tempHalfMatrix2[MATRIXSIZE * MATRIXSIZE / 4];
    static FLOAT_TYPE tempHalfMatrix3[MATRIXSIZE * MATRIXSIZE / 4];
    
    
    static FLOAT_TYPE P_k_Mult_H_Transponse[MATRIXSIZE / 2 * 9];
    static FLOAT_TYPE temp_Div[9 * 9];
    static FLOAT_TYPE temp_Div2[9 * 9];
    
    static FLOAT_TYPE b_z_k[9];
    static FLOAT_TYPE K_Mult_z[MATRIXSIZE / 2];
    static FLOAT_TYPE sum;
	static int i;
    compute_xdot_and_F__ins_ekf_quaternion();
    
    /*  Use VanLoan Method to convert from linearized continuous-domain state  */
    /*  model (F,Q_t), into discrete-domain state transformation (PHI_k & Q_k). */
    /*  Thus, assuming u(t) is relatively constant between times k and k+1, it  */
    /*  converts */
    /*    xdot(t)=F*x(t)+Fu*u(t)+w(t), E[w(t)*w(t)']=Q(t)  */
    /*  to */
    /*    x(k+1)=PHI(k)*x(k)+PHIu(k)*u(k)+w(k), E[w(k)*w(k)']=Q(k). */
    /*  */
    /*  VanLoan Method: */
    /*  */
    /*        [ -F(t) | Q(t) ] */
    /*   AA = [-------+------]*dt */
    /*        [  zero | F(t)'] */
    /*  */
    /*                   [ ... | inv(PHI(k))*Q(k)] */
    /*   BB = expm(AA) = [-----+-----------------] */
    /*                   [ zero|      PHI(k)'    ] */
    /*  */
    /*  Thus, PHI(k) is the transpose of the lower-right of BB.  Using the upper */
    /*  right of BB, Q(k) = PHI(k) * inv(PHI(k))*Q(k).  The derivation of PHIu is  */
    /*  not provided because it is not needed. */
    
    
//    memset( AA, 0, sizeof( FLOAT_TYPE )*MATRIXSIZE * MATRIXSIZE ); // AA = 0
    // Fill -F(t)
    MatrixOperations::copySquareToPartOfSquare<FLOAT_TYPE>( AA, F, MATRIXSIZE, MATRIXSIZE / 2, 0, 0 );
//	MatrixOperations::multPart<FLOAT_TYPE>( AA, MATRIXSIZE, MATRIXSIZE / 2, MATRIXSIZE / 2, 0, 0, -1.0 );
    //Fill Q(t)
    MatrixOperations::copySquareToPartOfSquare<FLOAT_TYPE>( AA,  Q, MATRIXSIZE, MATRIXSIZE / 2, 0, MATRIXSIZE / 2 );
    //Fill F(t)'
    MatrixOperations::copyTranposingSquareToPartOfSquare<FLOAT_TYPE>( AA, F, MATRIXSIZE, MATRIXSIZE / 2, MATRIXSIZE / 2, MATRIXSIZE / 2 );
    //We don't need F from now
	//Multiply dt
//	MatrixOperations::multPart<FLOAT_TYPE>( AA, MATRIXSIZE, MATRIXSIZE, MATRIXSIZE, 0, 0, dt );
	MatrixOperations::multPart<FLOAT_TYPE>( AA, MATRIXSIZE, MATRIXSIZE / 2, MATRIXSIZE / 2, 0, 0, -dt );
	MatrixOperations::multPart<FLOAT_TYPE>( AA, MATRIXSIZE, MATRIXSIZE / 2, MATRIXSIZE / 2, 0, MATRIXSIZE / 2, dt );
	MatrixOperations::multPart<FLOAT_TYPE>( AA, MATRIXSIZE, MATRIXSIZE / 2, MATRIXSIZE / 2, MATRIXSIZE / 2, MATRIXSIZE / 2, dt );
    
    /*  <- Matrix exponential! */
    //При меньшем порядке набегает слишком большая ошибка!
    //не использовать меньший порядок!!!
	ExpmMath::fexpm<MATRIXSIZE, 1>( AA, BB );

	/* 'perform_ekf:228' PHI_k = BB(nStates+1:2*nStates,nStates+1:2*nStates)'; */
    MatrixOperations::copyTransposingPartOfSquareToSquare<FLOAT_TYPE>( tempHalfMatrix1, BB, MATRIXSIZE / 2, MATRIXSIZE, MATRIXSIZE / 2, MATRIXSIZE / 2 ); // tempHalfMatrix1 = PHI_k
    /* 'perform_ekf:229' Q_k = PHI_k*BB(1:nStates,nStates+1:2*nStates); */
	MatrixOperations::copyPartOfSquareToSquare<FLOAT_TYPE>( tempHalfMatrix2, BB, MATRIXSIZE / 2, MATRIXSIZE, 0, MATRIXSIZE / 2 );
	MatrixOperations::multSquareMatrixes < FLOAT_TYPE, MATRIXSIZE / 2 , sizeof( FLOAT_TYPE )*MATRIXSIZE* MATRIXSIZE / 4 > ( tempHalfMatrix1, tempHalfMatrix2, F ); //F = Q_k now
    
    /*  Predict state estimate xhat forward dt seconds. */
    /*  */
    /*  We use Euler integration on the non-linear state derivatives,  */
    /*  xdot(t)=f(x(t),u(t)), to propagate xhat dt seconds from time k to time */
    /*  k+1: */
    /*     xhat(k+1) = xhat(k) + xdot(t)*dt. */
    /*  We alternatively could have used: */
    /*     xhat(k+1) = PHI*xhat(k) + PHIu*u(k). */
    /*  Doing so, however, would have introduced some linearization error and */
    /*  required us to compute PHIu. */
    
    /* 'perform_ekf:241' xhat_k = xhat_k + xdot*dt; */
    for ( i = 0; i < MATRIXSIZE / 2; i++ ) {
        xhat[i] += xdot[i] *  dt;
    }
    
    /*  Predict state covariance (P_k) forward dt seconds. */
    /*    PHI_k: State transition matrix from time k to time k+1 */
    /*    Q_k:   Discrete-time process noise matrix */
    /*  */
    /*  Note: In the P_k equation below, the PHI_k*P_k*PHI_k' term directly */
    /*  propagates the "a priori" state uncertainty from time k to time k+1.  The */
    /*  Q_k term adds additional uncertainty at each time step due to the process */
    /*  noise, w(k). */
    /*     P(k+1) = PHI(k)*P(k)*PHI(k)' + Q(k) */
    /* 'perform_ekf:252' P_k = PHI_k*P_k*PHI_k' + Q_k; */
    
    MatrixOperations::multSquareMatrixes < FLOAT_TYPE, MATRIXSIZE / 2, sizeof( FLOAT_TYPE )*MATRIXSIZE* MATRIXSIZE / 4 > ( tempHalfMatrix1,  P, tempHalfMatrix2 );
    MatrixOperations::multSquareMatrixesTransposingRight < FLOAT_TYPE, sizeof( FLOAT_TYPE )*MATRIXSIZE* MATRIXSIZE / 4 > ( tempHalfMatrix2, tempHalfMatrix1, tempHalfMatrix3, MATRIXSIZE / 2 );
    MatrixOperations::addSquareMatrixes<FLOAT_TYPE>( tempHalfMatrix3, F,  P, MATRIXSIZE / 2 ); //after that don't need Q_k
    
    bool z_empty = true;
    
    for ( i = 0; i < 9; i++ ) {
        if ( z[i] != 0.0 ) {
            z_empty = false;
            break;
        }
    }
    
    if ( !z_empty ) { // -----New measurments------
        compute_zhat_and_H__ins_ekf_quaternion();
        
        /*  K: Kalman gain matrix */
        /*   K = P_k*H'*inv(H*P_k*H'+R_k); */
        /*  */
        /*  Note: in Matlab, inv() can be very slow and inaccurate, so we'll use */
        /*  the forward-slash ("/", see "help mrdivide") instead.   */
        /*  See Reference 4 for a potentially more efficient Cholesky  */
        /*  Factorization method for computing K, xhat_k & P_k. */
        /* 'perform_ekf:273' K = (P_k*H')/(H*P_k*H'+R_k); */
        
        MatrixOperations::multMatrixesRightTransponse < FLOAT_TYPE, sizeof( FLOAT_TYPE )*MATRIXSIZE / 2 * 9 > ( P, H, P_k_Mult_H_Transponse, MATRIXSIZE / 2, MATRIXSIZE / 2, 9 );
        MatrixOperations::multMatrixes<FLOAT_TYPE, sizeof( FLOAT_TYPE ) * 9 * 9>( H, P_k_Mult_H_Transponse, temp_Div, 9, MATRIXSIZE / 2, 9 );
        MatrixOperations::addSquareMatrixes<FLOAT_TYPE>( temp_Div,  R , temp_Div2, 9 );
        MatrixOperations::invSquareMatrix<FLOAT_TYPE, 9, sizeof( FLOAT_TYPE ) * 9 * 9>( temp_Div2, temp_Div ); // tempDiv = 1/(H*P_k*H'+R_k);
        
        MatrixOperations::multMatrixes < FLOAT_TYPE, sizeof( FLOAT_TYPE )*MATRIXSIZE / 2 * 9 > ( P_k_Mult_H_Transponse, temp_Div, K, MATRIXSIZE / 2, 9, 9 );
        
        /*  Update of state estimates using measurement residual (z-zhat). */
        /*  */
        /*  Effectively, K provides a means weighting how much the measurement */
        /*  should affect the state estimate, xhat.  Thus, if the measurement  */
        /*  uncertainty (R_k) is "large" compared with the state uncertainty (P_k) */
        /*  then gain matrix K will be "small", meaning that the new measurement  */
        /*  will have little effect on the state estimate. */
        /*  In contrast, if the measurement uncertainty is small, then K will */
        /*  be large meaning we have high confidence in the measurement (z_k) and */
        /*  will weight it more than the predicted state estimate. */
        /* 'perform_ekf:284' xhat_k = xhat_k + K*(z_k-zhat); */
        for ( i = 0; i < 9; i++ ) {
            b_z_k[i] = z[i] - zhat[i];
        }
        
        MatrixOperations::multMatrixes < FLOAT_TYPE, sizeof( FLOAT_TYPE )*MATRIXSIZE / 2 * 1 > ( K, b_z_k, K_Mult_z, MATRIXSIZE / 2, 9, 1 );
        
        for ( i = 0; i < MATRIXSIZE / 2; i++ ) {
            xhat[i] += K_Mult_z[i];
        }
        
        
        /*  Update of state covariance matrix */
        /*  */
        /*  Effectively, this update reduces the state uncertainties due to  */
        /*  information gained in the measurement. */
        /*    P(k+1) = (I - K*H)*P(k+1)   (Note: I is the identity matrix) */
        /*           = P(k+1) - K*H*P(k+1) */
        
        /* 'perform_ekf:292' P_k = (eye(length(xhat_k))-K*H)*P_k; */
        
        memset( tempHalfMatrix1, 0, sizeof( FLOAT_TYPE )*MATRIXSIZE * MATRIXSIZE / 4 );
        
        for ( i = 0; i < MATRIXSIZE / 2; i++ ) {
            tempHalfMatrix1[i + i * MATRIXSIZE / 2] = 1;
        }
        
        MatrixOperations::multMatrixes < FLOAT_TYPE, sizeof( FLOAT_TYPE )*MATRIXSIZE* MATRIXSIZE / 4 > ( K, H, tempHalfMatrix2, MATRIXSIZE / 2, 9, MATRIXSIZE / 2 );
        MatrixOperations::multPart<FLOAT_TYPE>( tempHalfMatrix2, MATRIXSIZE / 2, MATRIXSIZE / 2, MATRIXSIZE / 2, 0, 0, -1.0 );
        MatrixOperations::addSquareMatrixes<FLOAT_TYPE>( tempHalfMatrix1, tempHalfMatrix2, tempHalfMatrix3, MATRIXSIZE / 2 );
		MatrixOperations::multSquareMatrixes < FLOAT_TYPE, MATRIXSIZE / 2 , sizeof( FLOAT_TYPE )*MATRIXSIZE* MATRIXSIZE / 4 > ( tempHalfMatrix3,  P, tempHalfMatrix1 );
        memcpy( P, tempHalfMatrix1, sizeof( FLOAT_TYPE )*MATRIXSIZE * MATRIXSIZE / 4 );
    }
    
    /*  Assign output values. */
    /*  These should be inputted back into perform_ekf at the subsequent time  */
    /*  step. */
    /* 'perform_ekf:299' xhat_new=xhat_k; */
    //memcpy(xhat_new, xhat,sizeof(FLOAT_TYPE)*MATRIXSIZE/2);
    /* P_new   =P_k;    % P(k+1) */
    //memcpy(P_new, P,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4);
    
    //------ Дополнительная нормировка коэффиц кватерниона------------
    sum = 0.0;
    
    for ( i = 0; i < 4; i++ ) {
        sum += xhat[i] * xhat[i];
    }
    
    sum = sqrt ( sum );
    
    for ( i = 0; i < 4; i++ ) {
        xhat[i] /= sum;
    }
}

void FilterData::compute_zhat_and_H__ins_ekf_quaternion()
{
    static FLOAT_TYPE M[9]; // Матрица перехода из географ. в собств.
    static FLOAT_TYPE C_mag2ned[9] = { 0.0, 0.0, 0.0,
                                       0.0, 0.0, 0.0,
                                       0.0, 0.0, 1.0
                                     }; // Матрица компенсации магнитного склонения
    static FLOAT_TYPE tempMatrix[9];
    
    static int i;
    
    
    //IdealField = {B * cos(MagneticDip), 0.0, B * sin(MagneticDip)};
    //#define MagneticDip 0
    //static const FLOAT_TYPE IdealField[3] = {1.0, 0.0, 0.0};
    
    //69.7
    //#define MagneticDip 1.2165
    static const FLOAT_TYPE IdealField[3] = {0.3469, 0.0, 0.9379};
    static const FLOAT_TYPE Hx = IdealField[0];
    static const FLOAT_TYPE Hy = IdealField[1];
    static const FLOAT_TYPE Hz = IdealField[2];
    
    static FLOAT_TYPE OutField[3];
    static FLOAT_TYPE cosMagDecl, sinMagDecl, MagDeclRad;
    
    MagDeclRad = MagDecl_deg * Deg2Rad;
    
    M[0] = 1.0 - 2.0 * ( xhat[2] * xhat[2] +  xhat[3] * xhat[3] );
    M[1] = 2.0 * ( xhat[1] *  xhat[2] +  xhat[3] *  xhat[0] );
    M[2] = 2.0 * ( xhat[1] *  xhat[3] -  xhat[2] *  xhat[0] );
    M[3] = 2.0 * ( xhat[1] *  xhat[2] -  xhat[3] *  xhat[0] );
    M[4] = 1.0 - 2.0 * ( xhat[1] * xhat[1] +  xhat[3] * xhat[3] );
    M[5] = 2.0 * ( xhat[2] *  xhat[3] +  xhat[1] *  xhat[0] );
    M[6] = 2.0 * ( xhat[1] *  xhat[3] +  xhat[2] *  xhat[0] );
    M[7] = 2.0 * ( xhat[2] *  xhat[3] -  xhat[1] *  xhat[0] );
    M[8] = 1.0 - 2.0 * ( xhat[1] * xhat[1] +  xhat[2] * xhat[2] );
    
    C_mag2ned[0] =  cos( MagDeclRad );
    C_mag2ned[1] = -sin( MagDeclRad );
    C_mag2ned[3] =  sin( MagDeclRad );
    C_mag2ned[4] =  cos( MagDeclRad );
    
    /* mag3D_unitVector_in_body = C_ned2b*C_mag2ned*IdealField; */
    MatrixOperations::multSquareMatrixes<FLOAT_TYPE, 3, sizeof( FLOAT_TYPE ) * 3 * 3>( M, C_mag2ned, tempMatrix );
    MatrixOperations::multMatrixes<FLOAT_TYPE, sizeof( FLOAT_TYPE ) * 3 * 1>( tempMatrix, const_cast<FLOAT_TYPE*>( IdealField ), OutField, 3, 3, 1 );
    
    //3D magnetometer unit vector
    zhat[0] = OutField[0];
    zhat[1] = OutField[1];
    zhat[2] = OutField[2];
    // Pn; Pe; Alt
    zhat[3] =  xhat[4];
    zhat[4] =  xhat[5];
    zhat[5] =  xhat[6];
    //Vn; Ve; Vd
    zhat[6] =  xhat[7];
    zhat[7] =  xhat[8];
    zhat[8] =  xhat[9];
    
    /* H - 16x9 */
    /*
    H =
    
    [                                                                             2*Hy*q3*cos((pi*mag_declination_deg)/180) - 2*Hz*q2 + 2*Hx*q3*sin((pi*mag_declination_deg)/180),                                                                                       2*Hz*q3 + 2*Hy*q2*cos((pi*mag_declination_deg)/180) + 2*Hx*q2*sin((pi*mag_declination_deg)/180), Hy*(2*q1*cos((pi*mag_declination_deg)/180) + 4*q2*sin((pi*mag_declination_deg)/180)) - Hx*(4*q2*cos((pi*mag_declination_deg)/180) - 2*q1*sin((pi*mag_declination_deg)/180)) - 2*Hz*q0, 2*Hz*q1 - Hx*(4*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)) + Hy*(2*q0*cos((pi*mag_declination_deg)/180) + 4*q3*sin((pi*mag_declination_deg)/180)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                             2*Hz*q1 - 2*Hx*q3*cos((pi*mag_declination_deg)/180) + 2*Hy*q3*sin((pi*mag_declination_deg)/180), 2*Hz*q0 + Hx*(2*q2*cos((pi*mag_declination_deg)/180) - 4*q1*sin((pi*mag_declination_deg)/180)) - Hy*(4*q1*cos((pi*mag_declination_deg)/180) + 2*q2*sin((pi*mag_declination_deg)/180)),                                                                                       2*Hz*q3 + 2*Hx*q1*cos((pi*mag_declination_deg)/180) - 2*Hy*q1*sin((pi*mag_declination_deg)/180), 2*Hz*q2 - Hx*(2*q0*cos((pi*mag_declination_deg)/180) + 4*q3*sin((pi*mag_declination_deg)/180)) - Hy*(4*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [ Hx*(2*q2*cos((pi*mag_declination_deg)/180) - 2*q1*sin((pi*mag_declination_deg)/180)) - Hy*(2*q1*cos((pi*mag_declination_deg)/180) + 2*q2*sin((pi*mag_declination_deg)/180)), Hx*(2*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)) - 4*Hz*q1 - Hy*(2*q0*cos((pi*mag_declination_deg)/180) + 2*q3*sin((pi*mag_declination_deg)/180)), Hx*(2*q0*cos((pi*mag_declination_deg)/180) + 2*q3*sin((pi*mag_declination_deg)/180)) - 4*Hz*q2 + Hy*(2*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)),           Hx*(2*q1*cos((pi*mag_declination_deg)/180) + 2*q2*sin((pi*mag_declination_deg)/180)) + Hy*(2*q2*cos((pi*mag_declination_deg)/180) - 2*q1*sin((pi*mag_declination_deg)/180)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
    */
    /*
    H =
    
    [                                                                             2*Hy*x[3]*cosMagDecl - 2*Hz*x[2] + 2*Hx*x[3]*sinMagDecl,                                                                                       2*Hz*x[3] + 2*Hy*x[2]*cosMagDecl + 2*Hx*x[2]*sinMagDecl, Hy*(2*x[1]*cosMagDecl + 4*x[2]*sinMagDecl) - Hx*(4*x[2]*cosMagDecl - 2*x[1]*sinMagDecl) - 2*Hz*x[0], 2*Hz*x[1] - Hx*(4*x[3]*cosMagDecl - 2*x[0]*sinMagDecl) + Hy*(2*x[0]*cosMagDecl + 4*x[3]*sinMagDecl), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                             2*Hz*x[1] - 2*Hx*x[3]*cosMagDecl + 2*Hy*x[3]*sinMagDecl, 2*Hz*x[0] + Hx*(2*x[2]*cosMagDecl - 4*x[1]*sinMagDecl) - Hy*(4*x[1]*cosMagDecl + 2*x[2]*sinMagDecl),                                                                                       2*Hz*x[3] + 2*Hx*x[1]*cosMagDecl - 2*Hy*x[1]*sinMagDecl, 2*Hz*x[2] - Hx*(2*x[0]*cosMagDecl + 4*x[3]*sinMagDecl) - Hy*(4*x[3]*cosMagDecl - 2*x[0]*sinMagDecl), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [ Hx*(2*x[2]*cosMagDecl - 2*x[1]*sinMagDecl) - Hy*(2*x[1]*cosMagDecl + 2*x[2]*sinMagDecl), Hx*(2*x[3]*cosMagDecl - 2*x[0]*sinMagDecl) - 4*Hz*x[1] - Hy*(2*x[0]*cosMagDecl + 2*x[3]*sinMagDecl), Hx*(2*x[0]*cosMagDecl + 2*x[3]*sinMagDecl) - 4*Hz*x[2] + Hy*(2*x[3]*cosMagDecl - 2*x[0]*sinMagDecl),           Hx*(2*x[1]*cosMagDecl + 2*x[2]*sinMagDecl) + Hy*(2*x[2]*cosMagDecl - 2*x[1]*sinMagDecl), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    [                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
    */
    
    memset( H, 0, sizeof( FLOAT_TYPE ) * 16 * 9 );
    
    sinMagDecl = sin( MagDeclRad );
    cosMagDecl = cos( MagDeclRad );
    
    H[0] = 2.0 * Hy * xhat[3] * cosMagDecl - 2.0 * Hz * xhat[2] + 2.0 * Hx * xhat[3] * sinMagDecl;
    H[1] = 2.0 * Hz * xhat[3] + 2.0 * Hy * xhat[2] * cosMagDecl + 2.0 * Hx * xhat[2] * sinMagDecl;
    H[2] = Hy * ( 2.0 * xhat[1] * cosMagDecl + 4.0 * xhat[2] * sinMagDecl ) - Hx * ( 4.0 * xhat[2] * cosMagDecl - 2.0 * xhat[1] * sinMagDecl ) - 2.0 * Hz * xhat[0];
    H[3] = 2.0 * Hz * xhat[1] - Hx * ( 4.0 * xhat[3] * cosMagDecl - 2.0 * xhat[0] * sinMagDecl ) + Hy * ( 2.0 * xhat[0] * cosMagDecl + 4.0 * xhat[3] * sinMagDecl );
    
    H[16] = 2.0 * Hz * xhat[1] - 2.0 * Hx * xhat[3] * cosMagDecl + 2.0 * Hy * xhat[3] * sinMagDecl;
    H[17] = 2.0 * Hz * xhat[0] + Hx * ( 2.0 * xhat[2] * cosMagDecl - 4.0 * xhat[1] * sinMagDecl ) - Hy * ( 4.0 * xhat[1] * cosMagDecl + 2.0 * xhat[2] * sinMagDecl );
    H[18] = 2.0 * Hz * xhat[3] + 2.0 * Hx * xhat[1] * cosMagDecl - 2.0 * Hy * xhat[1] * sinMagDecl;
    H[19] = 2.0 * Hz * xhat[2] - Hx * ( 2.0 * xhat[0] * cosMagDecl + 4.0 * xhat[3] * sinMagDecl ) - Hy * ( 4.0 * xhat[3] * cosMagDecl - 2.0 * xhat[0] * sinMagDecl );
    
    H[32] = Hx * ( 2.0 * xhat[2] * cosMagDecl - 2.0 * xhat[1] * sinMagDecl ) - Hy * ( 2.0 * xhat[1] * cosMagDecl + 2.0 * xhat[2] * sinMagDecl );
    H[33] = Hx * ( 2.0 * xhat[3] * cosMagDecl - 2.0 * xhat[0] * sinMagDecl ) - 4.0 * Hz * xhat[1] - Hy * ( 2.0 * xhat[0] * cosMagDecl + 2.0 * xhat[3] * sinMagDecl );
    H[34] = Hx * ( 2.0 * xhat[0] * cosMagDecl + 2.0 * xhat[3] * sinMagDecl ) - 4.0 * Hz * xhat[2] + Hy * ( 2.0 * xhat[3] * cosMagDecl - 2.0 * xhat[0] * sinMagDecl );
    H[35] = Hx * ( 2.0 * xhat[1] * cosMagDecl + 2.0 * xhat[2] * sinMagDecl ) + Hy * ( 2.0 * xhat[2] * cosMagDecl - 2.0 * xhat[1] * sinMagDecl );
    
    for ( i = 3; i < 9; i++ ) {
        H[ 16 * i + 4 + ( i - 3 ) ] = 1.0;
    }
}

void FilterData::compute_xdot_and_F__ins_ekf_quaternion()
{
    static FLOAT_TYPE M[9]; // Матрица перехода из собств. в географ.
    static FLOAT_TYPE g[3] = {0.0, 0.0,  GravityAccel_mps2}; // Вектор ускорения свободного падения в географ.
    static FLOAT_TYPE C_bodyrate2qdot[12];
    static FLOAT_TYPE compl_gyro[3]; // Комплексированные значения гироскопа
    static FLOAT_TYPE compl_accel[3]; // Комплексированные значения акселлерометра
	static int i;
    
    // .5*-q1  -q2  -q3; ...
    //     q0  -q3   q2; ...
    //     q3   q0  -q1; ...
    //    -q2   q1   q0
    C_bodyrate2qdot[0] = -0.5 * xhat[1];
    C_bodyrate2qdot[1] = -0.5 * xhat[2];
    C_bodyrate2qdot[2] = -0.5 * xhat[3];
    C_bodyrate2qdot[3] = 0.5 * xhat[0];
    C_bodyrate2qdot[4] = -0.5 * xhat[3];
    C_bodyrate2qdot[5] = 0.5 * xhat[2];
    C_bodyrate2qdot[6] = 0.5 * xhat[3];
    C_bodyrate2qdot[7] = 0.5 * xhat[0];
    C_bodyrate2qdot[8] = -0.5 * xhat[1];
    C_bodyrate2qdot[9] = -0.5 * xhat[2];
    C_bodyrate2qdot[10] = 0.5 * xhat[1];
    C_bodyrate2qdot[11] = 0.5 * xhat[0];
    
    //Transponse M
    M[0] = 1.0 - 2.0 * ( xhat[2] * xhat[2] +  xhat[3] * xhat[3] );
    M[3] = 2.0 * ( xhat[1] *  xhat[2] +  xhat[3] *  xhat[0] );
    M[6] = 2.0 * ( xhat[1] *  xhat[3] -  xhat[2] *  xhat[0] );
    M[1] = 2.0 * ( xhat[1] *  xhat[2] -  xhat[3] *  xhat[0] );
    M[4] = 1.0 - 2.0 * ( xhat[1] * xhat[1] +  xhat[3] * xhat[3] );
    M[7] = 2.0 * ( xhat[2] *  xhat[3] +  xhat[1] *  xhat[0] );
    M[2] = 2.0 * ( xhat[1] *  xhat[3] +  xhat[2] *  xhat[0] );
    M[5] = 2.0 * ( xhat[2] *  xhat[3] -  xhat[1] *  xhat[0] );
    M[8] = 1.0 - 2.0 * ( xhat[1] * xhat[1] +  xhat[2] * xhat[2] );
    
    for ( i = 0; i < 3; i++ ) {
        compl_gyro[i] =  gyro_rps[i] -  xhat[10 + i];
        compl_accel[i] =  accel_mps2[i] -  xhat[13 + i];
    }
    
    memset( xdot, 0, sizeof( FLOAT_TYPE ) * 16 );
    //q0 , q1, q2, q3
    MatrixOperations::multMatrixes<FLOAT_TYPE, sizeof( FLOAT_TYPE ) * 4 * 1>( C_bodyrate2qdot, compl_gyro, xdot, 4, 3, 1 );
    //Vn; Ve; -Vd
    xdot[4] =  xhat[7];
    xdot[5] =  xhat[8];
    xdot[6] = - xhat[9];
    MatrixOperations::multMatrixes<FLOAT_TYPE, sizeof( FLOAT_TYPE ) * 3 * 1>( M, compl_accel, xdot + 7, 3, 3, 1 );
    
    for ( i = 0; i < 3; i++ ) {
        xdot[7 + i] += g[i];
    }
    
    /*  Compute linearized state dynamics, F = d(xdot)/dx */
    /* F = [ ... */
    /* [                                 0,                                        bwx/2 - wx/2,                                        bwy/2 - wy/2,                                        bwz/2 - wz/2, 0, 0, 0, 0, 0,  0,  q1/2,  q2/2,  q3/2,                   0,                   0,                   0] */
    /* [                      wx/2 - bwx/2,                                                   0,                                        wz/2 - bwz/2,                                        bwy/2 - wy/2, 0, 0, 0, 0, 0,  0, -q0/2,  q3/2, -q2/2,                   0,                   0,                   0] */
    /* [                      wy/2 - bwy/2,                                        bwz/2 - wz/2,                                                   0,                                        wx/2 - bwx/2, 0, 0, 0, 0, 0,  0, -q3/2, -q0/2,  q1/2,                   0,                   0,                   0] */
    /* [                      wz/2 - bwz/2,                                        wy/2 - bwy/2,                                        bwx/2 - wx/2,                                                   0, 0, 0, 0, 0, 0,  0,  q2/2, -q1/2, -q0/2,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 1, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 1,  0,     0,     0,     0,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0, -1,     0,     0,     0,                   0,                   0,                   0] */
    /* [ 2*q3*(bay - fy) - 2*q2*(baz - fz),                 - 2*q2*(bay - fy) - 2*q3*(baz - fz), 4*q2*(bax - fx) - 2*q1*(bay - fy) - 2*q0*(baz - fz), 2*q0*(bay - fy) + 4*q3*(bax - fx) - 2*q1*(baz - fz), 0, 0, 0, 0, 0,  0,     0,     0,     0, 2*q2^2 + 2*q3^2 - 1,   2*q0*q3 - 2*q1*q2, - 2*q0*q2 - 2*q1*q3] */
    /* [ 2*q1*(baz - fz) - 2*q3*(bax - fx), 4*q1*(bay - fy) - 2*q2*(bax - fx) + 2*q0*(baz - fz),                 - 2*q1*(bax - fx) - 2*q3*(baz - fz), 4*q3*(bay - fy) - 2*q0*(bax - fx) - 2*q2*(baz - fz), 0, 0, 0, 0, 0,  0,     0,     0,     0, - 2*q0*q3 - 2*q1*q2, 2*q1^2 + 2*q3^2 - 1,   2*q0*q1 - 2*q2*q3] */
    /* [ 2*q2*(bax - fx) - 2*q1*(bay - fy), 4*q1*(baz - fz) - 2*q3*(bax - fx) - 2*q0*(bay - fy), 2*q0*(bax - fx) - 2*q3*(bay - fy) + 4*q2*(baz - fz),                 - 2*q1*(bax - fx) - 2*q2*(bay - fy), 0, 0, 0, 0, 0,  0,     0,     0,     0,   2*q0*q2 - 2*q1*q3, - 2*q0*q1 - 2*q2*q3, 2*q1^2 + 2*q2^2 - 1] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
    /* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
    
    memset( F, 0, sizeof( FLOAT_TYPE ) * 16 * 16 );
    
    //TODO 2.0 out of brackets
    // Row 0
    F[1] =  xhat[10] / 2.0 -  gyro_rps[0] / 2.0;
    F[2] =  xhat[11] / 2.0 -  gyro_rps[1] / 2.0;
    F[3] =  xhat[12] / 2.0 -  gyro_rps[2] / 2.0;
    
    F[10] =  xhat[1] / 2.0;
    F[11] =  xhat[2] / 2.0;
    F[12] =  xhat[3] / 2.0;
    
    // Row 1
    F[16] = ( gyro_rps[0] -  xhat[10] ) / 2.0;
    F[18] =  gyro_rps[2] / 2.0 -  xhat[12] / 2.0;
    F[19] =  xhat[11] / 2.0f -  gyro_rps[1] / 2.0;
    
    F[26] = - xhat[0] / 2.0;
    F[27] =  xhat[3] / 2.0;
    F[28] = - xhat[2] / 2.0;
    
    //Row 2
    F[32] =  gyro_rps[1] / 2.0 -  xhat[11] / 2.0;
    F[33] =  xhat[12] / 2.0 -  gyro_rps[2] / 2.0;
    F[35] =  gyro_rps[0] / 2.0 -  xhat[10] / 2.0;
    
    F[42] = - xhat[3] / 2.0;
    F[43] = - xhat[0] / 2.0;
    F[44] =  xhat[1] / 2.0;
    
    //Row 3
    F[48] =  gyro_rps[2] / 2.0f -  xhat[12] / 2.0;
    F[49] =  gyro_rps[1] / 2.0f -  xhat[11] / 2.0;
    F[50] =  xhat[10] / 2.0f -  gyro_rps[0] / 2.0;
    
    F[58] =  xhat[2] / 2.0;
    F[59] = - xhat[1] / 2.0;
    F[60] = - xhat[0] / 2.0;
    
    //Row 4
    F[71] = 1.0;
    
    //Row 5
    F[88] = 1.0;
    
    //Row 6
    F[105] = -1.0;
    
    //Row 7
    F[112] = 2.0 *  xhat[3] * ( xhat[14] -  accel_mps2[1] ) - 2.0 *  xhat[2] * ( xhat[15] -
             accel_mps2[2] );
    F[113] = -2.0 *  xhat[2] * ( xhat[14] -  accel_mps2[1] ) - 2.0 *  xhat[3] * ( xhat[15] -
             accel_mps2[2] );
    F[114] = ( 4.0 *  xhat[2] * ( xhat[13] -  accel_mps2[0] ) - 2.0 *  xhat[1] * ( xhat[14] -
               accel_mps2[1] ) ) - 2.0 *  xhat[0] * ( xhat[15] -  accel_mps2[2] );
    F[115] = ( 2.0 *  xhat[0] * ( xhat[14] -   accel_mps2[1] ) + 4.0 *  xhat[3] * ( xhat[13] -
               accel_mps2[0] ) ) - 2.0 *  xhat[1] * ( xhat[15] -   accel_mps2[2] );
               
    F[125] = ( 2.0 *  xhat[2] * xhat[2] + 2.0 *  xhat[3] * xhat[3] ) - 1.0;
    F[126] = 2.0 *  xhat[0] *  xhat[3] - 2.0 *  xhat[1] *  xhat[2];
    F[127] = -2.0 *  xhat[0] *  xhat[2] - 2.0 *  xhat[1] *  xhat[3];
    
    //Row 8
    F[128] = 2.0 *  xhat[1] * ( xhat[15] -   accel_mps2[2] ) - 2.0 *  xhat[3] * ( xhat[13] -
             accel_mps2[0] );
    F[129] = ( 4.0 *  xhat[1] * ( xhat[14] -   accel_mps2[1] ) - 2.0 *  xhat[2] * ( xhat[13] -
               accel_mps2[0] ) ) + 2.0 *  xhat[0] * ( xhat[15] -   accel_mps2[2] );
    F[130] = -2.0 *  xhat[1] * ( xhat[13] -   accel_mps2[0] ) - 2.0 *  xhat[3] * ( xhat[15] -
             accel_mps2[2] );
    F[131] = ( 4.0 *  xhat[3] * ( xhat[14] -   accel_mps2[1] ) - 2.0 *  xhat[0] * ( xhat[13] -
               accel_mps2[0] ) ) - 2.0 *  xhat[2] * ( xhat[15] -   accel_mps2[2] );
               
    F[141] = -2.0 *  xhat[0] *  xhat[3] - 2.0 *  xhat[1] *  xhat[2];
    F[142] = ( 2.0 *  xhat[1] * xhat[1] + 2.0 *  xhat[3] * xhat[3] ) - 1.0;
    F[143] = 2.0 *  xhat[0] *  xhat[1] - 2.0 *  xhat[2] *  xhat[3];
    
    //Row 9
    F[144] = 2.0 *  xhat[2] * ( xhat[13] -   accel_mps2[0] ) - 2.0 *  xhat[1] * ( xhat[14] -
             accel_mps2[1] );
    F[145] = ( 4.0 *  xhat[1] * ( xhat[15] -   accel_mps2[2] ) - 2.0 *  xhat[3] * ( xhat[13] -
               accel_mps2[0] ) ) - 2.0 *  xhat[0] * ( xhat[14] -   accel_mps2[1] );
    F[146] = ( 2.0 *  xhat[0] * ( xhat[13] -   accel_mps2[0] ) - 2.0 *  xhat[3] * ( xhat[14] -
               accel_mps2[1] ) ) + 4.0 *  xhat[2] * ( xhat[15] -   accel_mps2[2] );
    F[147] = -2.0 *  xhat[1] * ( xhat[13] -   accel_mps2[0] ) - 2.0 *  xhat[2] * ( xhat[14] -
             accel_mps2[1] );
             
    F[157] = 2.0 *  xhat[0] *  xhat[2] - 2.0 *  xhat[1] *  xhat[3];
    F[158] = -2.0 *  xhat[0] *  xhat[1] - 2.0 *  xhat[2] *  xhat[3];
    F[159] = ( 2.0 *  xhat[1] * xhat[1] + 2.0 *  xhat[2] * xhat[2] ) - 1.0;
}

void FilterData::SetInitX( FLOAT_TYPE InitQuat[4], FLOAT_TYPE InitPosition[3], FLOAT_TYPE InitVelocity[3] )
{
	static int i;

    for ( i = 0; i < 4; i++ ) {
        xhat[i] = InitQuat[i];
    }
    
    for ( i = 0; i < 3; i++ ) {
        xhat[i + 4] = InitPosition[i];
    }
    
    for ( i = 0; i < 3; i++ ) {
        xhat[i + 7] = InitVelocity[i];
    }
}

void FilterData::SetInitBias( FLOAT_TYPE InitBiasGyro[3], FLOAT_TYPE InitBiasAccel[3] )
{
	static int i;

    for ( i = 0; i < 3; i++ ) {
        xhat[i + 10] = InitBiasGyro[i];
    }
    
    for ( i = 0; i < 3; i++ ) {
        xhat[i + 13] = InitBiasAccel[i];
    }
}

void FilterData::FillInitSigmas()
{

//	sigmas.mag3D_unitVector_meas = 0.2;
//	sigmas.pos_meas_m            = 10.0;
//	sigmas.vel_meas_mps          = 1.0;
//	sigmas.gyro_bias_rps         = 0.06;
//	sigmas.accel_bias_mps2       =  0.2;
//	sigmas.quat_process_noise = 0.001;         // Quaternion process noise
//	sigmas.pos_process_noise_m = 1.0;           // Position process noise, m
//	sigmas.vel_process_noise_mps = 2.0;         // Velocity process noise, m/s
//	sigmas.gyroBias_process_noise_rps = 1e-5; // Gyro bias process noise, rad/s
//	sigmas.accelBias_process_noise_mps2 = 1e-5; // Accel bias process noise, m/s^2
//	sigmas.large_quat_uncertainty = 0.2;
//	sigmas.large_pos_uncertainty_m = 150.0;
//	sigmas.large_vel_uncertainty_mps = 10.0;

	sigmas.mag3D_unitVector_meas = 0.2;
	sigmas.pos_meas_m            = 5.0;
	sigmas.vel_meas_mps          = 2.0;
	sigmas.gyro_bias_rps         = 0.034;
	sigmas.accel_bias_mps2       =  0.1;
	sigmas.quat_process_noise = 0.005;         // Quaternion process noise
	sigmas.pos_process_noise_m = 10.0;           // Position process noise, m
	sigmas.vel_process_noise_mps = 5.0;         // Velocity process noise, m/s
	sigmas.gyroBias_process_noise_rps = 0.001; // Gyro bias process noise, rad/s
	sigmas.accelBias_process_noise_mps2 = 0.001; // Accel bias process noise, m/s^2
	sigmas.large_quat_uncertainty = 0.2;
	sigmas.large_pos_uncertainty_m = 150.0;
	sigmas.large_vel_uncertainty_mps = 10.0;

/*    sigmas.mag3D_unitVector_meas = 0.2;
    sigmas.pos_meas_m            = 10.0;
    sigmas.vel_meas_mps          = 1.0;
    sigmas.gyro_bias_rps         = 0.06;
    sigmas.accel_bias_mps2       =  0.2;
    sigmas.quat_process_noise = 0.02;         // Quaternion process noise
    sigmas.pos_process_noise_m = 7.0;           // Position process noise, m
    sigmas.vel_process_noise_mps = 2.0;         // Velocity process noise, m/s
    sigmas.gyroBias_process_noise_rps = 1e-3; // Gyro bias process noise, rad/s
    sigmas.accelBias_process_noise_mps2 = 1e-3; // Accel bias process noise, m/s^2
    sigmas.large_quat_uncertainty = 0.1;
    sigmas.large_pos_uncertainty_m = 150.0;
    sigmas.large_vel_uncertainty_mps = 10.0;*/
}

void FilterData::SetNewAccelData( FLOAT_TYPE Ax, FLOAT_TYPE Ay, FLOAT_TYPE Az )
{
    accel_mps2[0] = Ax;
    accel_mps2[1] = Ay;
    accel_mps2[2] = Az;
}

void FilterData::SetNewGyroData( FLOAT_TYPE Gx, FLOAT_TYPE Gy, FLOAT_TYPE Gz )
{
    gyro_rps[0] = Gx;
    gyro_rps[1] = Gy;
    gyro_rps[2] = Gz;
}

void FilterData::FillZMag( FLOAT_TYPE Mx, FLOAT_TYPE My, FLOAT_TYPE Mz )
{
    z[0] = Mx;
    z[1] = My;
    z[2] = Mz;
}

void FilterData::FillZPos( FLOAT_TYPE Nm, FLOAT_TYPE Em, FLOAT_TYPE Hm )
{
    z[3] = Nm;
    z[4] = Em;
    z[5] = Hm;
}

void FilterData::FillZVelocity( FLOAT_TYPE VN, FLOAT_TYPE VE, FLOAT_TYPE VH )
{
    z[6] = VN;
    z[7] = VE;
    z[8] = VH;
}

void FilterData::EmptyZR()
{
    memset( z, 0, sizeof( z ) );
    memset( R, 0, sizeof( R ) );
}

void FilterData::FillR()
{
	static int i;

    // R: sampled measurement uncertainty, R(k)
    for ( i = 0; i < 3; i++ ) {
        R[i + 9 * i] =  sigmas.mag3D_unitVector_meas * sigmas.mag3D_unitVector_meas;    // Mag. unit vector noise
    }
    
    for ( i = 3; i < 6; i++ ) {
        R[i + 9 * i] =  sigmas.pos_meas_m * sigmas.pos_meas_m;    // North-East-Alt position measurement noise, m
    }
    
    for ( i = 6; i < 9; i++ ) {
        R[i + 9 * i] =  sigmas.vel_meas_mps * sigmas.vel_meas_mps;    // NED velocity measurement noise, m/s
    }
    
    // Make sure no elements of R diagonal are zero! Arbitrarily use a
    // minimum of (1e-3)^2.
    for ( i = 0; i < 9; i++ ) {
        if (  R[i + 9 * i] < MATRIX_EPS ) {
            R[i + 9 * i] = MATRIX_EPS;
        }
    }
}

void FilterData::FillPQ()
{
	static int i;

    //Fill P
    // Start with "large" uncertainties and build initial covariance (state uncertainty)
    for ( i = 0; i < 4; i++ ) {
        P[i + 16 * i] =  sigmas.large_quat_uncertainty * sigmas.large_quat_uncertainty; // init quaternion (NED-to-body) uncertainty
        Q[i + 16 * i] =  sigmas.quat_process_noise * sigmas.quat_process_noise; // quaternion process noise
    }
    
    for ( i = 4; i < 7; i++ ) {
        P[i + 16 * i] =  sigmas.large_pos_uncertainty_m * sigmas.large_pos_uncertainty_m; // init North-East-Alt position uncertainties, m
        Q[i + 16 * i] =  sigmas.pos_process_noise_m * sigmas.pos_process_noise_m; // North-East-Alt position process noise, m
    }
    
    for ( i = 7; i < 10; i++ ) {
        P[i + 16 * i] =  sigmas.large_vel_uncertainty_mps * sigmas.large_vel_uncertainty_mps; // init NED velocity uncertainties, m/s
        Q[i + 16 * i] =  sigmas.vel_process_noise_mps * sigmas.vel_process_noise_mps; // NED velocity process noise, m/s
    }
    
    for ( i = 10; i < 13; i++ ) {
        P[i + 16 * i] =  sigmas.gyro_bias_rps * sigmas.gyro_bias_rps; // init XYZ gyro bias uncertainties, rad/s
        Q[i + 16 * i] =  sigmas.gyroBias_process_noise_rps * sigmas.gyroBias_process_noise_rps; // XYZ gyro bias process noise, rad/s
    }
    
    for ( i = 13; i < 16; i++ ) {
        P[i + 16 * i] =  sigmas.accel_bias_mps2 * sigmas.accel_bias_mps2; // init XYZ accel bias uncertainties, m/s^2
        Q[i + 16 * i] =  sigmas.accelBias_process_noise_mps2 * sigmas.accelBias_process_noise_mps2; // XYZ accel bias process noise, m/s^2
    }
    
    /*
     Make sure no elements of P diagonal are zero! Arbitrarily use a
     minimum of (1e-3)^2.  (For example, if gyroBias was set to zero when
     generating the sensor measurement, we still want some initial P.)
     */
    for ( i = 0; i < 16; i++ ) {
        if (  Q[i + 16 * i] < MATRIX_EPS ) {
            Q[i + 16 * i] = MATRIX_EPS;
        }
        
        if (  P[i + 16 * i] < MATRIX_EPS ) {
            P[i + 16 * i] = MATRIX_EPS;
        }
    }
}
