/**
 * @file encoder.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Июнь 2015 года
*/
#ifndef INCLUDE_ENCODER_H_
#define INCLUDE_ENCODER_H_

#include "stm32f4xx.h"
///@brief Обработка импульсов инкрементального энкодера
namespace EncoderName {
///@brief Тип используемого таймера для вычисления значения энкодера
enum Encoder_TypeDef:uint8_t
{
	Encoder_2, 	///< Таймер 2
	Encoder_3,	///< Таймер 3
	Encoder_5,	///< Таймер 5
	Encoder_8	///< Таймер 8
};
///@brief Обработка импульсов инкрементального энкодера
class Encoder
{
	float zeroAngle;	///< Начальный угол
	float angle;		///< Текуший угол
	float speed;		///< Скорость вращения двигателя
	uint64_t time;		///< Время предыдущего измерения
	uint32_t counter;	///< Значение счетчика импульсов
	float koef;			///< Коэффициент передаточного числа механики
	GPIO_TypeDef* port;	///< Указатель на порт выводов микроконтроллера
	TIM_TypeDef* timer;	///< Указатель на используемый таймер
public:
	Encoder(Encoder_TypeDef type, float koef, float zeroAngle = 0.f);
	void Update();
	float GetSpeed() {return speed;}
	float GetAngle() {return angle;}
	void SetZeroAngle(float angle) {zeroAngle = angle;}
};


}  // namespace EncoderName

#endif /* INCLUDE_ENCODER_H_ */
