#ifndef CONSTS_H
#define CONSTS_H

#include <cmath>

//static const double R_EARTH = 6371000.0; //Радиус Земли
//static const double R_EARTH_A = 6378245.0; //Радиус Земли (полуось a)
//static const double R_EARTH_B = 6356863.0; //Радиус Земли (полуось b)
//static const double R_EARTH_E = 0.08181;
//static const double R_PLANE_L = 0.84;
//static const double R_PLANE_W = 1.21;
//static /*const*/ double U_EARTH = 7.2921158553e-5; // Угловая скорость вращения Земли рад/с
static const double G_EARTH = 9.80665; //ускорение свободного падения у поверхности Земли
static const double Rad2Deg = 57.29577951308232;           // Radians to degrees conversion factor
static const double Deg2Rad = 0.01745329251994;          // Degrees to radians conversion factor
//static double G_CALC(double latitude, double hMSL) { //ускорение свободного падения в зависимости от положения
//    double temp = 1 + 0.0053024*sinf(latitude)*sinf(latitude)-0.0000058*sinf(latitude*2)*sinf(latitude*2);
//    temp = temp*9.780327-3.086e-6*hMSL;
//    return temp;
//}
#endif // CONSTS_H
