/**
 * @file osd.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Май 2015 года
*/

#ifndef INCLUDE_OSD_H_
#define INCLUDE_OSD_H_

#include "gpio.h"


///@brief Настройка и управление наложением текста на видео
namespace OsdName {

///@brief Настройка и управление наложением текста на видео
class Osd {
	GpioName::Gpio* pinSwitch;	///< Вывод переключения коммутаторов в обход OSD
	GpioName::Gpio* pinPower;	///< Вывод управления питанием
	GpioName::Gpio* pinCS;		///< Вывод Chip Select для SPI
	//need add SPI class
public:
	Osd(GpioName::GpioPort_TypeDef portSwitch, uint8_t pinSwitch,
			GpioName::GpioPort_TypeDef portPower, uint8_t pinPower,
			GpioName::GpioPort_TypeDef portCS, uint8_t pinCS);
};

}  // namespace OsdName



#endif /* INCLUDE_OSD_H_ */
