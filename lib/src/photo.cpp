/*
 * photo.cpp
 *
 *  Created on: 12 апр. 2015 г.
 *      Author: klan
 */
#include "../lib/include/photo.h"

using namespace PhotoName;
using namespace GpioName;


Photo::Photo(GpioPort_TypeDef portPower, uint8_t pinPower,
		GpioPort_TypeDef portFocus, uint8_t pinFocus,
		GpioPort_TypeDef portShot, uint8_t pinShot,
		GpioPort_TypeDef portZoomInc, uint8_t pinZoomInc,
		GpioPort_TypeDef portZoomDec, uint8_t pinZoomDec,
		GpioPort_TypeDef portZoomIncFast, uint8_t pinZoomIncFast,
		GpioPort_TypeDef portZoomDecFast, uint8_t pinZoomDecFast,
		GpioPort_TypeDef portUSBPower, uint8_t pinUSBPower,
		GpioPort_TypeDef portUSBControl, uint8_t pinUSBControl)
{
	zoomValue = 1;

	this->pinPower = new Gpio(portPower, pinPower, GpioMode_Normal);
	this->pinFocus = new Gpio(portFocus, pinFocus, GpioMode_Normal);
	this->pinShot = new Gpio(portShot, pinShot, GpioMode_Normal);
	this->pinZoomInc = new Gpio(portZoomInc, pinZoomInc, GpioMode_Normal);
	this->pinZoomDec = new Gpio(portZoomDec, pinZoomDec, GpioMode_Normal);
	this->pinZoomIncFast = new Gpio(portZoomIncFast, pinZoomIncFast, GpioMode_Normal);
	this->pinZoomDecFast = new Gpio(portZoomDecFast, pinZoomDecFast, GpioMode_Normal);
	this->pinUSBPower = new Gpio(portUSBPower, pinUSBPower, GpioMode_Normal);
	this->pinUSBControl = new Gpio(portUSBControl, pinUSBControl, GpioMode_Normal);

	shotTime = 0;
	shotFlag = false;

	burstTime = 0;
	burstFlag = false;


	focusFlag = false;

	zoomTime = 0;
	zoomFlag = false;

	videoNotUsed = false;
	sleepTime = 0;
	initBatteryTime = 0;
	initBatteryFlag = false;
	SetSleepDelaySec(60);	// 1 минута
}
Photo::~Photo()
{
	this->pinPower->~Gpio();
	this->pinFocus->~Gpio();
	this->pinShot->~Gpio();
	this->pinZoomInc->~Gpio();
	this->pinZoomDec->~Gpio();
	this->pinUSBPower->~Gpio();
	this->pinUSBControl->~Gpio();
}
void Photo::InitBattery()
{
	pinUSBPower->Switch(GpioState_On);
	DelayName::Delay::Wait(100000);
	pinUSBControl->Switch(GpioState_On);
	initBatteryTime = DelayName::Delay::GetMicrosecond();
	initBatteryFlag = true;

}
void Photo::Power(PhotoButton_TypeDef button)
{
	if(button == PhotoButton_On && !initBatteryFlag)
	{
		this->pinPower->Switch(GpioState_On);
		this->Focus(PhotoButton_Off);
		initTime = DelayName::Delay::GetMicrosecond();
		initFlag = true;
//		this->TestFoto();
	}
	else if(button == PhotoButton_Off)
	{
		this->pinPower->Switch(GpioState_Off);
		this->pinFocus->Switch(GpioState_Off);
		this->pinShot->Switch(GpioState_Off);
		this->pinZoomDec->Switch(GpioState_Off);
		this->pinZoomInc->Switch(GpioState_Off);
	}
}
void Photo::Focus(PhotoButton_TypeDef button)
{
	if(button == PhotoButton_On)
	{
		this->pinFocus->Switch(GpioState_On);
		focusFlag = true;
	}
	else if(button == PhotoButton_Off)
	{
		this->pinFocus->Switch(GpioState_Off);
		focusFlag = false;
	}
}
void Photo::TakeFoto()
{
	if(!focusFlag)
		this->Focus(PhotoButton_On);
	pinShot->Switch(GpioState_On);
	shotTime = DelayName::Delay::GetMicrosecond();
	shotFlag = true;
}
void Photo::TestFoto()
{
	if(!focusFlag)
		this->Focus(PhotoButton_On);
	pinShot->Switch(GpioState_On);
	shotTestTime = DelayName::Delay::GetMicrosecond();
	shotTestFlag = true;
}
void Photo::Zoom(uint8_t zoomValue)
{
	int deltaZoom = 0;
	if(!zoomFlag)
	{
		if((this->zoomValue == 1 && zoomValue == 3) ||
				(this->zoomValue == 3 && zoomValue == 1))
		{
			zoomDelay = 2600000;
			deltaZoom = zoomValue - this->zoomValue;
			this->zoomValue = zoomValue;
		}
		else if((this->zoomValue == 1 && zoomValue == 2) ||
				(this->zoomValue == 2 && zoomValue == 1))
		{
			zoomDelay = 1800000;
			deltaZoom = zoomValue - this->zoomValue;
			this->zoomValue = zoomValue;
		}
		else if((this->zoomValue == 2 && zoomValue == 3) ||
				(this->zoomValue == 3 && zoomValue == 2))
		{
			zoomDelay = 600000;
			deltaZoom = zoomValue - this->zoomValue;
			this->zoomValue = zoomValue;
		}

		if(deltaZoom > 0)
		{
			Focus(PhotoButton_Off);
			pinZoomInc->Switch(GpioState_On);
			zoomTime = DelayName::Delay::GetMicrosecond();
			zoomFlag = true;
		}
		else if(deltaZoom < 0)
		{
			Focus(PhotoButton_Off);
			pinZoomDec->Switch(GpioState_On);
			zoomTime = DelayName::Delay::GetMicrosecond();
			zoomFlag = true;
		}
	}
}

void Photo::VideoNotUsed(bool value)
{
	videoNotUsed = value;
	sleepTime = DelayName::Delay::GetMicrosecond();
}

void Photo::BurstMode(bool value, uint32_t delay)
{
	burstDelay = delay;
	burstFlag = value;
	if(value)
	{
		burstTime = DelayName::Delay::GetMicrosecond();
		TakeFoto();
	}
}

void Photo::Update(ConfigName::ConfigurationMUSV_TypeDef* configInput)
{
	if(shotFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - shotTime > 500000)
		{
			shotFlag = false;
//			pinFocus->Switch(GpioState_Off);
			pinShot->Switch(GpioState_Off);
		}
	}
	if(shotTestFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - shotTestTime > 2000000)
		{
			shotTestFlag = false;
			this->Focus(PhotoButton_Off);
			pinShot->Switch(GpioState_Off);
		}
	}
	if(burstFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - burstTime > burstDelay)
		{
			TakeFoto();
			burstTime = DelayName::Delay::GetMicrosecond();
		}
	}
	if(zoomFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - zoomTime > zoomDelay)
		{
			zoomFlag = false;
			pinZoomInc->Switch(GpioState_Off);
			pinZoomDec->Switch(GpioState_Off);
			Zoom(configInput->settings.zoom);
		}
	}
	if(videoNotUsed && !burstFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - sleepTime > sleepDelay)
		{
			pinPower->Switch(GpioState_Off);
		}
	}
	if(initBatteryFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - initBatteryTime > 10000000)
		{
			initBatteryFlag = false;
			pinUSBControl->Switch(GpioState_Off);
			pinUSBPower->Switch(GpioState_Off);
		}
	}
	if(initFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - initTime > 5000000)
		{
			initFlag = false;
			TestFoto();
		}
	}
}

