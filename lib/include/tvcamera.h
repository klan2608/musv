/**
 * @file tvcamera.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Июнь 2015 года
*/

#ifndef INCLUDE_TVCAMERA_H_
#define INCLUDE_TVCAMERA_H_

#include "usart.h"

/**
  * @brief Работа с ТВ камерами
  *
  *	Предназначен для работ с камерами Sony FCB.
  * Управляющие команды согластно протоколу Sony VISCO
  */
namespace TVCameraName {
/**
  * @brief  Класс для работы с ТВ камерами
  *
  *	Предназначен для работ с камерами Sony FCB.
  * Управляющие команды согластно протоколу Sony VISCO
  */
class TVCamera {
	UsartName::Usart* usartCamera; 	///< Указатель на интерфейс обмена данными
public:
	TVCamera(UsartName::Usart* usart);
	void Init();
	void SetZoom(uint8_t zoom);
	void Power(bool power);
};

}  // namespace TVCameraName


#endif /* INCLUDE_TVCAMERA_H_ */
