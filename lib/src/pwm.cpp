/*
 * pwm.cpp
 *
 *  Created on: 22 апр. 2015 г.
 *      Author: klan
 */

#include "../lib/include/pwm.h"

using namespace PwmName;

Pwm::Pwm(Pwm_TypeDef pwm, uint16_t period)
{
	state = PwmState_Init;

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

	TIM_TimeBaseInitTypeDef TIM_InitStructure;
	TIM_InitStructure.TIM_Prescaler = 0;
	TIM_InitStructure.TIM_Period = period;
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_InitStructure.TIM_RepetitionCounter = 0;

	TIM_OCInitTypeDef TIM_OCInitStructure;
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;

	TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
	TIM_BDTRStructInit(&TIM_BDTRInitStructure);
	TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
	TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
	TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_1;
	TIM_BDTRInitStructure.TIM_DeadTime = 80;
	TIM_BDTRInitStructure.TIM_Break = TIM_Break_Disable;
	TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;
	TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;


	switch(pwm)
	{
	//NEW
	case Pwm_2:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
		timer = TIM2;
		port = GPIOA;
		TIM_OC3Init(timer, &TIM_OCInitStructure);
		TIM_OC4Init(timer, &TIM_OCInitStructure);
		GPIO_PinAFConfig(port, GPIO_PinSource2, GPIO_AF_TIM2);
		GPIO_PinAFConfig(port, GPIO_PinSource3, GPIO_AF_TIM2);
		break;
	case Pwm_3:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
		timer = TIM3;
		port = GPIOC;
		TIM_OC3Init(timer, &TIM_OCInitStructure);
		TIM_OC4Init(timer, &TIM_OCInitStructure);
		GPIO_PinAFConfig(port, GPIO_PinSource9, GPIO_AF_TIM3);
		GPIO_PinAFConfig(port, GPIO_PinSource8, GPIO_AF_TIM3);
		break;
	//NEW
	case Pwm_5:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
		timer = TIM5;
		port = GPIOA;
		TIM_OC3Init(timer, &TIM_OCInitStructure);
		TIM_OC4Init(timer, &TIM_OCInitStructure);
		GPIO_PinAFConfig(port, GPIO_PinSource2, GPIO_AF_TIM5);
		GPIO_PinAFConfig(port, GPIO_PinSource3, GPIO_AF_TIM5);
		break;
	case Pwm_8:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
		timer = TIM8;
		port = GPIOC;
		TIM_OC3Init(timer, &TIM_OCInitStructure);
		TIM_OC4Init(timer, &TIM_OCInitStructure);
		GPIO_PinAFConfig(port, GPIO_PinSource8, GPIO_AF_TIM8);
		GPIO_PinAFConfig(port, GPIO_PinSource9, GPIO_AF_TIM8);
		TIM_BDTRConfig(timer, &TIM_BDTRInitStructure);
		break;
	default:
		state = PwmState_Error;
		break;
	}

	if(state == PwmState_Init)
	{
		GPIO_Init(port, &GPIO_InitStructure);
		TIM_TimeBaseInit(timer, &TIM_InitStructure);
		TIM_OC3PreloadConfig(timer, TIM_OCPreload_Enable);
		TIM_OC4PreloadConfig(timer, TIM_OCPreload_Enable);
		TIM_Cmd(timer, ENABLE);
		state = PwmState_On;
	}
}

void Pwm::SetCompare(uint16_t channel_3, uint16_t channel_4)
{
	TIM_SetCompare3(timer, channel_3);
	TIM_SetCompare4(timer, channel_4);
}
