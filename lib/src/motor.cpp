/*
 * motor.cpp
 *
 *  Created on: 03 мая 2015 г.
 *      Author: klan
 */
#include "../lib/include/motor.h"
#include "../lib/include/encoder.h"

using namespace MotorName;
using namespace PwmName;
using namespace EncoderName;


Motor::Motor(Pwm_TypeDef pwm, uint16_t period, Encoder_TypeDef encoder)
{
	if(period > 50000)
		this->period = 50000;
	else if(period < 1000)
		this->period = 1000;
	else
		this->period = period;

	this->pwm = new Pwm(pwm, this->period);
	this->encoder = new Encoder(encoder, 1/7/450);
	current = 0.f;
	currentADC = 0;
	koefADCtoCurrentConvert = 0.f;
	koefSpeed = this->period/200.f;
	direction = MotorDirection_Forward;

	Kp = 0.f;
	scopeKp = 0.f;
	Ki = 0.f;
	scopeKi = 0.f;
	Kd = 0.f;
	scopeKd = 0.f;
	L = 100.f;
	time = 0.1f;	// 100 Гц начальное значение
	integrator = 0.f;
	errorAngleCurrent = 0.f;
	errorAnglePrevious = 0.f;
	OutPID = 0.f;

	finishAngle = 0.f;
	angleKp = 0.1f;
}

Motor::~Motor()
{
	delete pwm;
}

void Motor::SetCurrentADC(uint16_t* current)
{
	currentADC = current;
}

void Motor::SetConvertKoef(float koef)
{
	koefADCtoCurrentConvert = koef;
}

void Motor::SetSpeed(float speed)
{
	speed = TrimSignalToScope(speed, 100.f);

	speed *= direction;

	pwm->SetCompare((uint16_t)(period/2 + speed*koefSpeed), (uint16_t)(period/2 - speed*koefSpeed));
}


float Motor::TrimSignalToScope ( float Signal, float Scope )
{
    if ( Signal < -Scope ) {
        return -Scope;
    }

    if ( Signal > Scope ) {
        return Scope;
    }
    else {
        return Signal;
    }
}

void Motor::UpdateInputAnglesRegulatorP(float finishAngle)
{
	this->finishAngle += (finishAngle - this->finishAngle) * angleKp;
}

void Motor::UpdatePID(float currentAngle, float finishAngle)
{
//	encoder->Update();
	if(state == MotorState_Run)
	{
		UpdateInputAnglesRegulatorP(finishAngle);

		errorAngleCurrent = this->finishAngle - currentAngle;

	//	if(errorAngleCurrent < 0.5f && errorAngleCurrent > -0.5f)
	//		integrator = 0.f;

		integrator += (errorAnglePrevious + errorAngleCurrent)/2*time;

		deriv = (errorAngleCurrent - errorAnglePrevious) / time;

		errorAnglePrevious = errorAngleCurrent;
		OutPID = (TrimSignalToScope(errorAngleCurrent*Kp, scopeKp) + TrimSignalToScope(integrator*Ki, scopeKi) + TrimSignalToScope(Kd*deriv, scopeKd));

		OutPID = TrimSignalToScope(OutPID, L);

		SetSpeed(OutPID);
	}
	else
		SetSpeed(0.f);
}
