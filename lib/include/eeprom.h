/**
 * @file eeprom.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Июнь 2015 года
*/

#ifndef INCLUDE_EEPROM_H_
#define INCLUDE_EEPROM_H_
#include "stm32f4xx.h"
#include "string.h"


///@brief Работа с эмуляцией EEPROM
namespace EepromName {

#define PAGE_SIZE               (uint32_t)0x20000  ///< Размер страницы памяти (256 кБ)
#define VOLTAGE_RANGE           (uint8_t)VoltageRange_3		///< Режим работы памяти
#define EEPROM_START_ADDRESS  ((uint32_t)0x080C0000) ///< Начальный адрес Flash памяти начиная с сектора 10 после использования 768 кБ памяти
#define PAGE0_BASE_ADDRESS    ((uint32_t)(EEPROM_START_ADDRESS + 0x0000)) ///< Адрес начала первой страницы памяти
#define PAGE0_END_ADDRESS     ((uint32_t)(EEPROM_START_ADDRESS + (PAGE_SIZE - 1))) ///< Адрес конца первой страницы памяти
#define PAGE0_ID               FLASH_Sector_10		///< Номер сектора первой страницы
#define PAGE1_BASE_ADDRESS    ((uint32_t)(EEPROM_START_ADDRESS + PAGE_SIZE))	///< Адрес начала второй страницы памяти
#define PAGE1_END_ADDRESS     ((uint32_t)(EEPROM_START_ADDRESS + (2 * PAGE_SIZE - 1))) ///< Адрес конца второй страницы памяти
#define PAGE1_ID               FLASH_Sector_11		///< Номер сектора второй страницы
#define PAGE0                 ((uint16_t)0x0000)	///< Индекс первой страницы
#define PAGE1                 ((uint16_t)0x0001)	///< Индекс второй страницы
#define NO_VALID_PAGE         ((uint16_t)0x00AB)	///< Память заполнена
#define ERASED                ((uint16_t)0xFFFF)     ///< Страница пустая
#define RECEIVE_DATA          ((uint16_t)0xEEEE)     ///< Страница помечена для приема данных
#define VALID_PAGE            ((uint16_t)0x0000)     ///< Страница имеет корректные данные
#define READ_FROM_VALID_PAGE  ((uint8_t)0x00)
#define WRITE_IN_VALID_PAGE   ((uint8_t)0x01)
#define PAGE_FULL             ((uint8_t)0x80)
#define NB_OF_VAR             ((uint8_t)0x03)


///@brief Эмуляция EEPROM
class Eeprom
{
	static uint8_t numberOfUser;				///< Количество выделленных ID
	static uint8_t numberOfWordsForUser[32];	///< Количество переменных, выделенных для каждого ID
	static uint16_t numberOfAddress;			///< Количество всех выделленых виртуальных адресов
	static uint16_t* address;					///< Указатель на массив виртуальных адресов
	static uint16_t indexCurrent;				///< Текщий индекс виртуального адреса в массиве

	static uint16_t PageTransfer(uint16_t VirtAddress, uint16_t Data);
	static uint16_t VerifyPageFullWriteVariable(uint16_t VirtAddress, uint16_t Data);
	static uint16_t FindValidPage(uint8_t Operation);
	static FLASH_Status Format(void);
	static uint16_t ReadValue(uint16_t VirtAddress, uint16_t* Data);
	static uint16_t WriteValue(uint16_t VirtAddress, uint16_t Data);
public:
	static uint16_t DataVar;

	static void Init();
	static uint16_t InitMemory();
	static uint8_t ReserveMemory(uint8_t number);
	static void Read(uint8_t userID, uint16_t indexOfVariable, float* data);
	static void Write(uint8_t userID, uint16_t indexOfVariable, float data);
};

}  // namespace EepromName


#endif /* INCLUDE_EEPROM_H_ */
