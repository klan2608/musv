/*
 * imu.cpp
 *
 *  Created on: May 14, 2015
 *      Author: yura
 */

#include "../lib/include/imu.h"

using namespace ImuName;
using namespace SensorName;
using namespace SensorRegMapName;
using namespace FilterName;
using namespace DelayName;
using namespace I2cName;
using namespace GpioName;
using namespace EepromName;


Imu::Imu(I2c_TypeDef i2c, uint16_t frequency, ImuMag_TypeDef magType, uint8_t address)
{
	this->i2c = new I2c(i2c);
	this->address = address;
	this->magType = magType;
	temperature = 20.f;
	iterator = 0;
	time = 0;
	initialization = true;
	calibration = false;
	accel = new Sensor(BIT_AFS_SEL_4G, 9.81f / 8192.f);
	accel->SetAxisDirection(SensorAxisDirection_Inverted, SensorAxisDirection_Inverted, SensorAxisDirection_Normal);
	gyro = new Sensor(BIT_FS_SEL_1000DPS, 1.0f / 32.8f * Deg2Rad);
	gyro->SetAxisDirection(SensorAxisDirection_Inverted, SensorAxisDirection_Inverted, SensorAxisDirection_Normal);
	mag = new Sensor(HMC5883L_GAIN_1090, 1.0f / 1090);
	if(magType == ImuMag_Internal)
	{
		mag->RemapAxis(SensorAxis_Y, SensorAxis_X, SensorAxis_Z);
		mag->SetAxisDirection(SensorAxisDirection_Inverted, SensorAxisDirection_Inverted, SensorAxisDirection_Inverted);
	}
	else
	{
		mag->RemapAxis(SensorAxis_Y, SensorAxis_X, SensorAxis_Z);
		mag->SetAxisDirection(SensorAxisDirection_Inverted, SensorAxisDirection_Normal, SensorAxisDirection_Normal);
	}
//	accel = new Sensor(BIT_AFS_SEL_4G, 9.81f / 8192.f);
//	accel->RemapAxis(SensorAxis_Z, SensorAxis_Y, SensorAxis_X);
//	accel->SetAxisDirection(SensorAxisDirection_Normal, SensorAxisDirection_Inverted, SensorAxisDirection_Normal);
//	gyro = new Sensor(BIT_FS_SEL_1000DPS, 1.0f / 32.8f * Deg2Rad);
//	gyro->RemapAxis(SensorAxis_Z, SensorAxis_Y, SensorAxis_X);
//	gyro->SetAxisDirection(SensorAxisDirection_Normal, SensorAxisDirection_Inverted, SensorAxisDirection_Normal);
//	mag = new Sensor(HMC5883L_GAIN_1090, 1.0f / 1090);
//	if(magType == ImuMag_Internal)
//	{
//		mag->RemapAxis(SensorAxis_Y, SensorAxis_X, SensorAxis_Z);
//		mag->SetAxisDirection(SensorAxisDirection_Inverted, SensorAxisDirection_Inverted, SensorAxisDirection_Inverted);
//	}
//	else
//	{
//		mag->RemapAxis(SensorAxis_Z, SensorAxis_X, SensorAxis_Y);
//		mag->SetAxisDirection(SensorAxisDirection_Normal, SensorAxisDirection_Normal, SensorAxisDirection_Normal);
//	}
	gyroInit = new Axis<float>;

	gyroInit->X = 0.1f;
	gyroInit->Y = 0.1f;
	gyroInit->Z = 0.1f;

	accel->GetValue()->X = 0.1f;
	accel->GetValue()->Y = 0.1f;
	accel->GetValue()->Z = 0.1f;

	gyro->GetValue()->X = 0.1f;
	gyro->GetValue()->Y = 0.1f;
	gyro->GetValue()->Z = 0.1f;

	mag->GetValue()->X = 0.1f;
	mag->GetValue()->Y = 0.1f;
	mag->GetValue()->Z = 0.1f;

	state = ImuState_Correct;

	Init();
//	pinInterrupt = new Gpio(GpioPort_D, 2, GpioMode_Int_Falling_OD);
}

void Imu::UpdateValue()
{
//	if(accel->DataReady())
//	{
//		accel->SetDataReady(false);
		if(CheckConection() == ImuState_Correct)
		{
			if(state == ImuState_Error)
				Init();

			if(state == ImuState_Correct)
			{
				time = Delay::GetMicrosecond() - timeLast;
				timeLast = Delay::GetMicrosecond();

				static uint8_t readDataMPU[14];
				static uint8_t readDataMag[15];

				if(!i2c->ReadData(address, address, ACCEL_XOUT_H, readDataMPU, 14))
				{
					state = ImuState_Error;
					return;
				}

				accel->SetAxisValue(SensorAxis_X, readDataMPU[0], readDataMPU[1]);
				accel->SetAxisValue(SensorAxis_Y, readDataMPU[2], readDataMPU[3]);
				accel->SetAxisValue(SensorAxis_Z, readDataMPU[4], readDataMPU[5]);

				gyro->SetAxisValue(SensorAxis_X, readDataMPU[8], readDataMPU[9]);
				gyro->SetAxisValue(SensorAxis_Y, readDataMPU[10], readDataMPU[11]);
				gyro->SetAxisValue(SensorAxis_Z, readDataMPU[12], readDataMPU[13]);

				if(!i2c->ReadData(address, address, EXT_SENS_DATA_00, readDataMag, 15))
				{
					state = ImuState_Error;
					return;
				}

				if(magType == ImuMag_ExternalHMC)
				{
					mag->SetDataReady(readDataMag[0] & AK8975C_STATUS1_READY);
					mag->SetAxisValue(SensorAxis_X, readDataMag[7], readDataMag[8]);
					mag->SetAxisValue(SensorAxis_Z, readDataMag[9], readDataMag[10]);
					mag->SetAxisValue(SensorAxis_Y, readDataMag[11], readDataMag[12]);
				}
				else
				{
					static float sensitivity[3];
					for(int i = 0; i < 3; i++)
						sensitivity[i] = ((readDataMag[i] - 128)*0.5f/128.f + 1.f)*0.01f;

					mag->SetKoef(sensitivity[0], sensitivity[1], sensitivity[2]);

					mag->SetDataReady(readDataMag[5] & 0x01);
					mag->SetAxisValue(SensorAxis_X, readDataMag[7], readDataMag[6]);
					mag->SetAxisValue(SensorAxis_Y, readDataMag[9], readDataMag[8]);
					mag->SetAxisValue(SensorAxis_Z, readDataMag[11], readDataMag[10]);
				}


				if(initialization)
				{
					iterator++;
					gyroInit->X += gyro->GetValue()->X;
					gyroInit->Y += gyro->GetValue()->Y;
					gyroInit->Z += gyro->GetValue()->Z;
					if(iterator > 100)
					{
						initialization = false;
						gyroInit->X /= iterator;
						gyroInit->Y /= iterator;
						gyroInit->Z /= iterator;
						ekf.InitFilter(accel, gyroInit, mag);
					}
				}
				else if(calibration)
				{
					if(mag->GetValue()->X > calibData.max.X)
						calibData.max.X = mag->GetValue()->X;
					else if(mag->GetValue()->X < calibData.min.X)
							calibData.min.X = mag->GetValue()->X;

					if(mag->GetValue()->Y > calibData.max.Y)
						calibData.max.Y = mag->GetValue()->Y;
					else if(mag->GetValue()->Y < calibData.min.Y)
							calibData.min.Y = mag->GetValue()->Y;

					if(mag->GetValue()->Z > calibData.max.Z)
						calibData.max.Z = mag->GetValue()->Z;
					else if(mag->GetValue()->Z < calibData.min.Z)
							calibData.min.Z = mag->GetValue()->Z;
				}
				else
				{

					ekf.Update(accel, gyro, mag, 0.020f);
				}
			}
		}
		else
			state = ImuState_Error;
//	}
}

void Imu::Init()
{
	bool allDataSend = true;
	//Init MPU9150
	allDataSend = i2c->WriteData(address, MPUREG_PWR_MGMT_1, BIT_H_RESET);
	Delay::Wait(250000);
	allDataSend &= i2c->WriteData(address, MPUREG_PWR_MGMT_1, BIT_CYCLE);
	Delay::Wait(250000);
	allDataSend &= i2c->WriteData(address, MPUREG_PWR_MGMT_1, BIT_CLKSEL_PLL_EXT19MHz);
	Delay::Wait(250000);
	allDataSend &= i2c->WriteData(address, MPUREG_PWR_MGMT_2, 0x00);
	Delay::Wait(250000);
	allDataSend &= i2c->WriteData(address, MPUREG_SMPLRT_DIV, SAMPLE_RATE_50Hz);
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, MPUREG_CONFIG, BIT_DLPF_CFG_42Hz);
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, MPUREG_ACCEL_CONFIG, BIT_AFS_SEL_4G);
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, MPUREG_GYRO_CONFIG, BITS_FS_1000DPS);
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, MPUREG_PWR_MGMT_1, BIT_CLKSEL_PLL_EXT19MHz);
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, MPUREG_INT_PIN_CFG, BIT_INT_ANYRD_2CLEAR);
	Delay::Wait(200);

//	//Init MPU9150 AUX I2C
	allDataSend &= i2c->WriteData(address, MPUREG_INT_PIN_CFG, 0x12); //i2c bypass on
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, MPUREG_USER_CTRL, 0x00);		//i2c master mode disable
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, MPUREG_INT_PIN_CFG, BIT_INT_ANYRD_2CLEAR);	//i2c bypass off
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, I2C_MST_CTRL, BIT_I2C_MST_P_NSR | BIT_I2C_MST_CLK_400kHz);	//P_NSR, 400kHz
	Delay::Wait(200);

	if(magType == ImuMag_ExternalHMC)
	{
		//read MAG
		allDataSend &= i2c->WriteData(address, I2C_SLV0_ADDR, I2C_SLV0_R | HMC5883_ADDRESS);	//READ!!!!!!!!!!!!
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV0_REG, 0x09);	//reg (status)
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV0_CTRL, I2C_SLV0_EN | 13);	//Slave0 enable 13 bytes
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV1_ADDR, HMC5883_ADDRESS);	//WRITE
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV1_REG, 0x02);	//reg (MODE) 0x02		//CHANGE!!!!!! for self test in registr 0x00 set value 0x11
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV1_CTRL, I2C_SLV1_EN | 1);	//Slave1 enable 1 byte
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV1_DO, 0x01);	//reg value (single measurement) 0x01
		Delay::Wait(200);
		//delays, MASTER enable
		allDataSend &= i2c->WriteData(address, I2C_SLV4_CTRL, 0);	//MST_DLY = 25/1 = 25 [Hz]
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_MST_DELAY_CTRL, BIT_DELAY_ES_SHADOW | BIT_I2C_SLV3_DLY_EN | BIT_I2C_SLV1_DLY_EN);	//delay slaves
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, USER_CTRL, BIT_I2C_MST_EN);	//MASTER enable
		Delay::Wait(200);
	}
	else
	{
		//read MAG
		allDataSend &= i2c->WriteData(address, I2C_SLV0_ADDR, I2C_SLV0_R | AK8975C_ADDRESS);	//READ!!!!!!!!!!!!
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV0_REG, 0x10);	//reg (ID) 0x00
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV0_CTRL, I2C_SLV0_EN | 3);	//Slave0 enable 10 bytes
		Delay::Wait(200);

		allDataSend &= i2c->WriteData(address, I2C_SLV1_ADDR, I2C_SLV1_R | AK8975C_ADDRESS);	//READ!!!!!!!!!!!!
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV1_REG, AK8975C_ID);	//reg (ID) 0x00
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV1_CTRL, I2C_SLV1_EN | 10);	//Slave0 enable 10 bytes
		Delay::Wait(200);

		allDataSend &= i2c->WriteData(address, I2C_SLV2_ADDR, AK8975C_ADDRESS);	//WRITE
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV2_REG, AK8975C_CONTROL);	//reg (MODE) 0x0A
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV2_CTRL, I2C_SLV2_EN | 1);	//Slave1 enable 1 byte
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_SLV2_DO, AK8975C_CONTROL_SINGLE);	//reg value (single measurement) 0x01
		Delay::Wait(200);
		//delays, MASTER enable
		allDataSend &= i2c->WriteData(address, I2C_SLV4_CTRL, 0);	//MST_DLY = 25/1 = 25 [Hz]
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, I2C_MST_DELAY_CTRL, BIT_DELAY_ES_SHADOW | BIT_I2C_SLV3_DLY_EN | BIT_I2C_SLV2_DLY_EN);	//delay slaves
		Delay::Wait(200);
		allDataSend &= i2c->WriteData(address, USER_CTRL, BIT_I2C_MST_EN);	//MASTER enable
		Delay::Wait(200);
	}


	//interrupts
	allDataSend &= i2c->WriteData(address, INT_PIN_CFG, 0xF0);	//interrupt LATCH_INT_EN
	Delay::Wait(200);
	allDataSend &= i2c->WriteData(address, INT_ENABLE, 0x00);	//interrupt DATA_DRY_EN
	Delay::Wait(1000);

	initialization = true;
	iterator = 0;

	if(allDataSend)
	{
		Delay::Wait(100);
		this->state = ImuState_Correct;
	}
	else
	{
		Delay::Wait(100);
		this->state = ImuState_Error;
	}
}

ImuState_TypeDef Imu::CheckConection()
{
	if(i2c->ReadByte(address, WHO_AM_I) == 0x68)
	{
		return ImuState_Correct;
	}
	else
	{
		return ImuState_Error;
	}
}

void Imu::ReadCalibrationDataFromMemory()
{
	Eeprom::Read(eepromID, 0, &calibData.bias.X);
	Eeprom::Read(eepromID, 1, &calibData.bias.Y);
	Eeprom::Read(eepromID, 2, &calibData.bias.Z);
	Eeprom::Read(eepromID, 3, &calibData.scale.X);
	Eeprom::Read(eepromID, 4, &calibData.scale.Y);
	Eeprom::Read(eepromID, 5, &calibData.scale.Z);

	bool correctData = true;
	correctData &= (calibData.bias.X < 1.f) & (calibData.bias.X > -1.f);
	correctData &= (calibData.bias.Y < 1.f) & (calibData.bias.Y > -1.f);
	correctData &= (calibData.bias.Z < 1.f) & (calibData.bias.Z > -1.f);
	correctData &= (calibData.scale.X < 3.f) & (calibData.scale.X > 1.f);
	correctData &= (calibData.scale.Y < 3.f) & (calibData.scale.Y > 1.f);
	correctData &= (calibData.scale.Z < 3.f) & (calibData.scale.Z > 1.f);

	if(!correctData)
	{
		SetCalibration(true);
	}

	mag->SetBias(calibData.bias.X, calibData.bias.Y, calibData.bias.Z);
	mag->SetScale(calibData.scale.X, calibData.scale.Y, calibData.scale.Z);
}

void Imu::SetCalibration(bool value)
{
	calibration = value;
	if(value)
	{
		calibData.max.X = -1.f;
		calibData.max.Y = -1.f;
		calibData.max.Z = -1.f;
		calibData.min.X = 1.f;
		calibData.min.Y = 1.f;
		calibData.min.Z = 1.f;
		calibData.bias.X = 0.f;
		calibData.bias.Y = 0.f;
		calibData.bias.Z = 0.f;
		calibData.scale.X = 1.f;
		calibData.scale.Y = 1.f;
		calibData.scale.Z = 1.f;

		mag->SetBias(calibData.bias.X, calibData.bias.Y, calibData.bias.Z);
		mag->SetScale(calibData.scale.X, calibData.scale.Y, calibData.scale.Z);
	}
	else
	{
		calibData.bias.X = (calibData.min.X + calibData.max.X) / 2;
		calibData.bias.Y = (calibData.min.Y + calibData.max.Y) / 2;
		calibData.bias.Z = (calibData.min.Z + calibData.max.Z) / 2;

		calibData.scale.X = 2.f / (calibData.max.X - calibData.min.X);
		calibData.scale.Y = 2.f / (calibData.max.Y - calibData.min.Y);
		calibData.scale.Z = 2.f / (calibData.max.Z - calibData.min.Z);

		bool correctData = true;
		correctData &= (calibData.bias.X < 1.f) & (calibData.bias.X > -1.f);
		correctData &= (calibData.bias.Y < 1.f) & (calibData.bias.Y > -1.f);
		correctData &= (calibData.bias.Z < 1.f) & (calibData.bias.Z > -1.f);
		correctData &= (calibData.scale.X < 3.f) & (calibData.scale.X > 1.f);
		correctData &= (calibData.scale.Y < 3.f) & (calibData.scale.Y > 1.f);
		correctData &= (calibData.scale.Z < 3.f) & (calibData.scale.Z > 1.f);

		if(correctData)
		{
			mag->SetBias(calibData.bias.X, calibData.bias.Y, calibData.bias.Z);
			mag->SetScale(calibData.scale.X, calibData.scale.Y, calibData.scale.Z);

			Eeprom::Write(eepromID, 0, calibData.bias.X);
			Eeprom::Write(eepromID, 1, calibData.bias.Y);
			Eeprom::Write(eepromID, 2, calibData.bias.Z);
			Eeprom::Write(eepromID, 3, calibData.scale.X);
			Eeprom::Write(eepromID, 4, calibData.scale.Y);
			Eeprom::Write(eepromID, 5, calibData.scale.Z);
		}
		else
			SetCalibration(true);
	}
}



void Imu::ResetAllData()
{
	accel->ResetData();
	gyro->ResetData();
	mag->ResetData();
}

void Imu::Handler()
{
	accel->SetDataReady(true);
	gyro->SetDataReady(true);
}
