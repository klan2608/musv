/**
 * @file main.cpp
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Апрель 2015 года
*/


#include "stm32f4xx.h"
#include "../lib/include/delay.h"
#include "../lib/include/usart.h"
#include "../lib/include/photo.h"
#include "../lib/include/motor.h"
#include "../lib/include/protocol.h"
#include "../lib/include/imu.h"
#include "../lib/include/nmea.h"
#include "../lib/include/video.h"
#include "../lib/include/eeprom.h"
#include "../lib/include/encoder.h"

#define MPU9150_ADDR 0xD2

using namespace MotorName;
using namespace UsartName;
using namespace DelayName;
using namespace ProtocolName;
using namespace ImuName;
using namespace NMEAName;
using namespace PhotoName;
using namespace VideoName;
using namespace OsdName;
using namespace GpioName;
using namespace ConfigName;
using namespace PwmName;
using namespace I2cName;
using namespace EepromName;
using namespace EncoderName;


Gpio* ledRed;
Gpio* ledYellow;
Gpio* ledGreen;
Motor* rollMotor;
Usart* uartUAV;
Protocol* protocolMUSV;
Nmea* nmeaOutput;
Imu* binsMUSV;
Photo* photo;
Video* video;
Osd* osd;

bool flagParcking = false;

RCC_ClocksTypeDef RCC_Clocks;
uint64_t time = 0, time2 = 0;
uint8_t iter = 0;

void UpdateConfig(ConfigurationMUSV_TypeDef* configInput);

int main()
{
	SystemCoreClockUpdate();
	RCC_GetClocksFreq(&RCC_Clocks);

	Delay::Init();
	Delay::Wait(2000000);

	Eeprom::Init();

	ledYellow = new Gpio(GpioPort_C, 11, GpioMode_Inverted_OD);
	ledGreen = new Gpio(GpioPort_C, 10, GpioMode_Inverted_OD);
	ledRed = new Gpio(GpioPort_A, 15, GpioMode_Inverted_OD);

	photo = new Photo(GpioPort_C, 0, 	/* Power Button       */
			GpioPort_C, 13,          	/* Focus Button       */
			GpioPort_C, 14, 			/* Shot Button        */
			GpioPort_B, 5, 				/* ZoomInc Button     */
			GpioPort_C, 15,  			/* ZoomDec Button     */
			GpioPort_B, 4, 				/* ZoomIncFast Button */
			GpioPort_B, 3,  			/* ZoomDecFast Button */
			GpioPort_B, 12, 			/* USBPower Button    */
			GpioPort_B, 0   			/* USBControl Button  */
			);

	photo->InitBattery();

	rollMotor = new Motor(Pwm_8, 6000, Encoder_3);
	rollMotor->SetDirection(MotorDirection_Revers);
	rollMotor->SetTime(0.020f);

	video = new Video(GpioPort_C, 1,
			GpioPort_B, 10,
			new Osd(GpioPort_C, 5,
					GpioPort_C, 2,
					GpioPort_C, 4));

	uartUAV = new Usart(Usart_1, 115200);
	protocolMUSV = new Protocol(128);
	protocolMUSV->ReserveMemoryEeprom();
	nmeaOutput = new Nmea();

	protocolMUSV->GetConfig()->settings.cameraMode = SettingCameraMode_TV;
	protocolMUSV->GetConfig()->newData.cameraModeChange = true;
	protocolMUSV->GetConfig()->settings.zoom = 1;
	protocolMUSV->GetConfig()->newData.zoomChange = true;
	protocolMUSV->GetConfig()->settings.photoMode.mode = PhotoMode_NoShot;
	protocolMUSV->GetConfig()->newData.photoModeChange = true;
//	protocolMUSV->GetConfig()->configPID.roll.Kp = 4.f;
//	protocolMUSV->GetConfig()->configPID.roll.scopeKp = 60.f;
//	protocolMUSV->GetConfig()->configPID.roll.Ki = 0.5f;
//	protocolMUSV->GetConfig()->configPID.roll.scopeKi = 40.f;
//	protocolMUSV->GetConfig()->configPID.roll.Kd = 0.3f;
//	protocolMUSV->GetConfig()->configPID.roll.scopeKd = 90.f;
//	protocolMUSV->GetConfig()->newData.pidChange = true;

	binsMUSV = new Imu(I2c1, 10, ImuMag_ExternalHMC, MPU9150_ADDR);
	binsMUSV->ReserveMemoryEeprom();
	binsMUSV->GetEkf()->ReserveMemoryEeprom();
	//Калибровка на столе
//	binsMUSV->GetMag()->SetBias(0.0925f, -0.0175f, -0.6125f);
//	binsMUSV->GetMag()->SetScale(1.67364f, 1.64609f, 1.716764f);

	//Калибровка на камере датчик на фотоаппарате
//	binsMUSV->GetMag()->SetBias(0.29f, 2.79f, -0.775f);
//	binsMUSV->GetMag()->SetScale(1.7316f, 1.25786f, 1.7094f);

	//Калибровка на камере датчик на обтекателе на стойке в пормальном положении
//	binsMUSV->GetMag()->SetBias(-0.11f, 0.25f, 0.47f);
//	binsMUSV->GetMag()->SetScale(1.754386f, 1.8518519f, 1.851819f);

	//Калибровка на камере датчик на обтекателе на стойке в противоположном положении
//	binsMUSV->GetMag()->SetBias(-0.105f, 0.2725f, 0.455f);
//	binsMUSV->GetMag()->SetScale(1.7094017f, 1.68776f, 1.76991f);

	Eeprom::InitMemory();

	protocolMUSV->ReadConfigFromMemory();
	UpdateConfig(protocolMUSV->GetConfig());

	binsMUSV->ReadCalibrationDataFromMemory();
	binsMUSV->GetEkf()->ReadDataFromMemory();

	char data[] = "$disable,\n";
	uartUAV->SendData((uint8_t*) data, sizeof(data)-1);

	while (1)
    {
		time = Delay::GetMicrosecond();
		iter++;

		protocolMUSV->DecodingData(uartUAV->GetReadData(), uartUAV->GetReadDataSize());
		UpdateConfig(protocolMUSV->GetConfig());
		photo->Update(protocolMUSV->GetConfig());

		binsMUSV->UpdateValue();

		switch(iter)
		{
		case 1:
			if(binsMUSV->GetState() == ImuState_Correct)
				ledGreen->Switch(GpioState_On);
			else
				ledRed->Switch(GpioState_On);
			break;
		case 5:
			ledGreen->Switch(GpioState_Off);
			ledRed->Switch(GpioState_Off);
			break;
		case 50:
			iter = 0;
			break;
		default:
			break;
		}

//		nmeaOutput->GenerateTime(Delay::GetMicrosecond(), 0, 0, 0,0,0);
//		nmeaOutput->GenerateKVANDWithoutCRC(binsMUSV->GetAngles());
//		nmeaOutput->GenerateIMU(binsMUSV);
		protocolMUSV->CodingOutputAngleData(binsMUSV->GetAngles()->roll, binsMUSV->GetAngles()->pitch, binsMUSV->GetAngles()->yaw);
//		protocolMUSV->CodingOutputAngleData(binsMUSV->GetAngles()->roll, binsMUSV->GetAngles()->pitch, binsMUSV->GetAngles()->yaw);
//		protocolMUSV->CodingOutputAngleData(binsMUSV->GetAngles()->roll, (float)(protocolMUSV->GetTail()*1.00000f), (float)(protocolMUSV->GetHead()*1.00000f));
//		uartUAV->SendData((uint8_t*) &uartUAV->GetReadDataSize(), 2);
//		uartUAV->SendData((uint8_t*) uartUAV->GetReadData(), uartUAV->GetReadDataSize());
//		uartUAV->SendData((uint8_t*) nmeaOutput->GetBuffer(), nmeaOutput->GetSize());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());

		if(protocolMUSV->GetConfig()->inputAngles.roll < -90.f && binsMUSV->GetAngles()->roll < -170.f && !flagParcking)
		{
			flagParcking = true;
			photo->Power(PhotoButton_Off);
			video->Switch(VideoMode_TV);
		}
		else if(protocolMUSV->GetConfig()->inputAngles.roll >= -90.f && flagParcking)
			flagParcking = false;

		if(binsMUSV->GetState() == ImuState_Correct && !binsMUSV->GetCalibration() && !flagParcking)
			rollMotor->UpdatePID(binsMUSV->GetAngles()->roll, protocolMUSV->GetConfig()->inputAngles.roll);
		else
			rollMotor->SetSpeed(0.f);

//		rollMotor->UpdateEncoder();

		while((Delay::GetMicrosecond() - time) < 20000);
    }
}


void UpdateConfig(ConfigurationMUSV_TypeDef* configInput)
{
	if(configInput->newData.cameraModeChange)
	{
		switch(configInput->settings.cameraMode)
		{
		case SettingCameraMode_Photo:
			video->Switch(VideoMode_Photo);
			photo->VideoNotUsed(false);
			photo->Power(PhotoButton_On);
			break;
		case SettingCameraMode_TV:
			photo->VideoNotUsed(true);
			video->Switch(VideoMode_TV);
			break;
		default:
			break;
		}
		configInput->newData.cameraModeChange = false;
	}
	if(configInput->newData.photoModeChange)
	{
		switch(configInput->settings.photoMode.mode)
		{
		case PhotoMode_Single:
			photo->TakeFoto();
			//photo->BurstMode(false, 0);
			configInput->settings.photoMode.mode = PhotoMode_NoShot;
			break;
		case PhotoMode_Burst:
			photo->BurstMode(true, (uint32_t) (configInput->settings.photoMode.delay*10000));
			break;
		case PhotoMode_NoShot:
			photo->BurstMode(false, 0);
			break;
		default:
			break;
		}
		configInput->newData.photoModeChange = false;
	}
	if(configInput->newData.focusChange)
	{
		switch(configInput->settings.focus)
		{
		case SettingFocus_Off:
			photo->Focus(PhotoButton_Off);
			break;
		case SettingFocus_Auto:
		case SettingFocus_Infinity:
			photo->Focus(PhotoButton_On);
			break;
		}
		configInput->newData.focusChange = false;
	}
	if(configInput->newData.zoomChange)
	{
		photo->Zoom(configInput->settings.zoom);
		configInput->newData.zoomChange = false;
	}
	if(configInput->newData.calibrationChange)
	{
		binsMUSV->SetCalibration(configInput->settings.calibration);
		configInput->newData.calibrationChange = false;
	}
	if(configInput->newData.pidChange)
	{
		rollMotor->SetPIDKp(configInput->configPID.roll.Kp);
		rollMotor->SetPIDScopeKp(configInput->configPID.roll.scopeKp);
		rollMotor->SetPIDKi(configInput->configPID.roll.Ki);
		rollMotor->SetPIDScopeKi(configInput->configPID.roll.scopeKi);
		rollMotor->SetPIDKd(configInput->configPID.roll.Kd);
		rollMotor->SetPIDScopeKd(configInput->configPID.roll.scopeKd);
		rollMotor->SetPIDL(configInput->configPID.roll.L);
		configInput->newData.pidChange = false;
	}
	if(configInput->configPID.roll.request)
	{
		configInput->configPID.roll.request = false;
		protocolMUSV->CodingOutputPID(MSG_REG_PID_ROLL_Kp, rollMotor->GetPIDKp());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
		protocolMUSV->CodingOutputPID(MSG_REG_PID_ROLL_ScopeKp, rollMotor->GetPIDScopeKp());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
		protocolMUSV->CodingOutputPID(MSG_REG_PID_ROLL_Ki, rollMotor->GetPIDKi());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
		protocolMUSV->CodingOutputPID(MSG_REG_PID_ROLL_ScopeKi, rollMotor->GetPIDScopeKi());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
		protocolMUSV->CodingOutputPID(MSG_REG_PID_ROLL_Kd, rollMotor->GetPIDKd());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
		protocolMUSV->CodingOutputPID(MSG_REG_PID_ROLL_ScopeKd, rollMotor->GetPIDScopeKd());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
		protocolMUSV->CodingOutputPID(MSG_REG_PID_ROLL_L, rollMotor->GetPIDL());
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
	}
	if(configInput->newData.requestCalibData)
	{
		configInput->newData.requestCalibData = false;
		protocolMUSV->CodingOutputCalibData(binsMUSV->GetCalibData().bias.X, binsMUSV->GetCalibData().bias.Y, binsMUSV->GetCalibData().bias.Z,
				binsMUSV->GetCalibData().scale.X, binsMUSV->GetCalibData().scale.Y, binsMUSV->GetCalibData().scale.Z);
		uartUAV->SendData((uint8_t*) protocolMUSV->GetData(), protocolMUSV->GetDataSize());
	}
	if(configInput->newData.sensorZero)
	{
		configInput->newData.sensorZero = false;
		binsMUSV->GetEkf()->SetAnglesBias(binsMUSV->GetAnglesRaw()->roll,
				binsMUSV->GetAnglesRaw()->pitch,
				binsMUSV->GetAnglesRaw()->yaw);
		binsMUSV->GetEkf()->WriteDataToMemory();
	}
	if(configInput->newData.motorStateChange == true)
	{
		configInput->newData.motorStateChange = false;
		if(configInput->settings.motorState)
			rollMotor->SetState(MotorState_Run);
		else
			rollMotor->SetState(MotorState_Stop);
	}

}

extern "C"
{

//void DMA1_Stream3_IRQHandler(void)
//{
//	if (DMA_GetITStatus(DMA1_Stream3, DMA_IT_TCIF3))
//	{
//		uartCamera->dataSendingComplete = true;
//		DMA_Cmd(DMA1_Stream3, DISABLE);
//		DMA_ClearITPendingBit(DMA1_Stream3, DMA_IT_TCIF3);
//		uartCamera->StartTransmite();
//	}
//}
void DMA2_Stream7_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA2_Stream7, DMA_IT_TCIF7))
	{
		uartUAV->dataSendingComplete = true;
		DMA_Cmd(DMA2_Stream7, DISABLE);
		DMA_ClearITPendingBit(DMA2_Stream7, DMA_IT_TCIF7);
		uartUAV->StartTransmite();
	}
}

void EXTI2_IRQHandler(void)
{
	ledYellow->Toggle();
	binsMUSV->Handler();
	EXTI_ClearITPendingBit(EXTI_Line2);
}
}

