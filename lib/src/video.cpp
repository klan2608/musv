/*
 * video.cpp
 *
 *  Created on: May 26, 2015
 *      Author: yura
 */

#include "../lib/include/video.h"

using namespace VideoName;
using namespace GpioName;
using namespace OsdName;




Video::Video(GpioPort_TypeDef portSwitch, uint8_t pinSwitch,
		GpioPort_TypeDef portCameraPower, uint8_t pinCameraPower,
		Osd* osd)
{
	pinVideoSwitch = new Gpio(portSwitch, pinSwitch, GpioMode_Normal);
	this->pinCameraPower = new Gpio(portCameraPower, pinCameraPower, GpioMode_Normal);
	this->pinCameraPower->Switch(GpioState_On);
	this->osd = osd;
}


void Video::Switch(VideoMode_TypeDef type)
{
	if(type == VideoMode_TV)
	{
		pinVideoSwitch->Switch(GpioState_On);
	}
	else if(type == VideoMode_Photo)
	{
		pinVideoSwitch->Switch(GpioState_Off);
	}
}
