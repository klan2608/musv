#ifndef STRCONV_H
#define STRCONV_H

#include <stdint.h>
#include <cmath>

namespace Conv {

static int32_t StoI(uint8_t *str, uint8_t size) {
	if (size == 0)
		return 0;

	int n, mul = 1;
	for (n = 0; n < size; n++) {
		if ((str[n] < '0') || (str[n] > '9')) {
			if ((str[n] != '-'))
				return 0;
		}
	}
	uint8_t SymCount = 0;
	if (*str == '-') {
		mul = -1;
		str++;
		size--;
	}
	for (n = 0; (*str >= '0') && (*str <= '9') && (SymCount < size);
			SymCount++, str++)
		n = n * 10 + *str - '0';
	return n * mul;
}

static float StoF(uint8_t *FloatStr, uint8_t size) {

	if (size == 0)
		return 0;
	float CellPart, FractPart, div = 1;
	uint8_t i = 0;
	int8_t FrPos = -1;
	for (i = 0; i < size; i++)
		if (FloatStr[i] == '.') {
			FrPos = i;
			break;
		}
	if (FrPos < 0) {
		//есть только целая часть
		return StoI(FloatStr, size);
	} else {
		CellPart = StoI(FloatStr, FrPos); //вычисленная целая часть
		//рассмотрим дробную часть
		for (i = 0; i < size - FrPos - 1; i++)
			div *= 10.0;
		FractPart = StoI(FloatStr + FrPos + 1, size - FrPos - 1) / div; //дробная часть
		if (CellPart < 0)
			return CellPart - FractPart;
		else
			return CellPart + FractPart;
	}
}

using namespace std;

static uint8_t ItoS(int32_t in, char*FinalStr) {
	if (FinalStr == nullptr)
		return 0;
	int32_t temp,temp2;
	uint8_t Uplim = 20, Lowlim = 0, i = 0, k = 1;

	if (in < 0) {
		temp = abs(in);
		FinalStr[0] = '-';
		Lowlim = 1;
	} else {
		temp = in;
	}
	temp2 = temp;

	while (i <= Uplim) {
		++i;
		if (temp % 10 != 0)
			k = i;
		temp /= 10;
	}

	i = k + Lowlim; // количество знаков и цифр

	temp = temp2;
	while (i > Lowlim) {
		FinalStr[i - 1] = temp % 10 + '0';
		temp /= 10;
		i--;
	}

	return k + Lowlim;
}

static uint8_t FtoS(float in, char*str, uint8_t pres) {

	if (str == nullptr)
		return 0;
	uint8_t i = 0, Lowlim = 0;
	float ftemp;

	if (in < 0) {
		ftemp = fabs(in);
		str[0] = '-';
		Lowlim = 1;
	} else
		ftemp = in;

	uint16_t temp = (uint16_t) ftemp;
	uint8_t size1 = ItoS(temp, str + Lowlim);
	size1 += Lowlim;
	ftemp = ftemp - temp;
	str[size1++] = '.';

	while (i <= pres) {
		ftemp *= 10;
		str[size1++] = ((uint16_t) ftemp % 10) + '0';
		++i;
	}
	return size1;
}
}

#endif // STRCONV_H
