/*
 * nmea.cpp
 *
 *  Created on: May 18, 2015
 *      Author: yura
 */

#include "../lib/include/nmea.h"
#include "../lib/include/TypeConverter.h"

using namespace NMEAName;
using namespace ImuName;
using namespace SensorName;
using namespace FilterName;
using namespace Conv;

Nmea::Nmea() {
	size = sizeof(buffer);
	memset(buffer, 0, size);
}

void Nmea::GenerateTime(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minut, uint32_t second)
{
	memset(buffer, 0, sizeof(buffer));

	size = 0;
	memcpy(buffer + size, "$GBTMR,", sizeof("$GBGYR,") - 1);
	size += sizeof("$GBGYR,") - 1;
	size += ItoS(year, buffer + size);
	buffer[size++] = '/';
	size += ItoS(month, buffer + size);
	buffer[size++] = '/';
	size += ItoS(day, buffer + size);
	buffer[size++] = ' ';
	size += ItoS(hour, buffer + size);
	buffer[size++] = ':';
	size += ItoS(minut, buffer + size);
	buffer[size++] = ':';
	size += ItoS(second, buffer + size);
	memcpy(buffer + size, "*00\n", sizeof("*00\n") - 1);
	size += sizeof("*00\n") - 1;
}

void Nmea::GenerateIMU(Imu* imu) {
	memset(buffer, 0, sizeof(buffer));

	size = 0;
	memcpy(buffer + size, "$GBGYR,", sizeof("$GBGYR,") - 1);
	size += sizeof("$GBGYR,") - 1;
	size += FtoS(imu->GetGyro()->GetValue()->X, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetGyro()->GetValue()->Y, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetGyro()->GetValue()->Z, buffer + size, 3);
	memcpy(buffer + size, "*00\n", sizeof("*00\n") - 1);
	size += sizeof("*00\n") - 1;
	//---------------------
	memcpy(buffer + size, "$GBACC,", sizeof("$GBACC,") - 1);
	size += sizeof("$GBACC,") - 1;
	size += FtoS(imu->GetAccel()->GetValue()->X, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetAccel()->GetValue()->Y, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetAccel()->GetValue()->Z, buffer + size, 3);
	memcpy(buffer + size, "*00\n", sizeof("*00\n") - 1);
	size += sizeof("*00\n") - 1;
	//---------------------
	memcpy(buffer + size, "$GBMAG,", sizeof("$GBMAG,") - 1);
	size += sizeof("$GBMAG,") - 1;
	size += FtoS(imu->GetMag()->GetValue()->X, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetMag()->GetValue()->Y, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetMag()->GetValue()->Z, buffer + size, 3);
	memcpy(buffer + size, "*00\n", sizeof("*00\n") - 1);
	size += sizeof("*00\n") - 1;

	//---------------------
	memcpy(buffer + size, "$GBTMP,", sizeof("$GBTMP,") - 1);
	size += sizeof("$GBTMP,") - 1;
	size += FtoS(imu->GetTemperature(), buffer + size, 3);
	memcpy(buffer + size, "*00\n", sizeof("*00\n") - 1);
	size += sizeof("*00\n") - 1;

	//---------------------
	memcpy(buffer + size, "$GBTMR,", sizeof("$GBTMR,") - 1);
	size += sizeof("$GBTMR,") - 1;
	size += ItoS(imu->GetTime(), buffer + size);
	memcpy(buffer + size, "*00\n", sizeof("*00\n") - 1);
	size += sizeof("*00\n") - 1;

	//---------------------
	memcpy(buffer + size, "$GBANG,", sizeof("$GBANG,") - 1);
	size += sizeof("$GBANG,") - 1;
	size += FtoS(imu->GetEkf()->GetAngles()->roll, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetEkf()->GetAngles()->pitch, buffer + size, 3);
	buffer[size++] = ',';
	size += FtoS(imu->GetEkf()->GetAngles()->yaw, buffer + size, 3);
	memcpy(buffer + size, "*00\n", sizeof("*00\n") - 1);
	size += sizeof("*00\n") - 1;

//	sprintf(buffer,
//			"$GBGYR,%f,%f,%f*00\n$GBACC,%f,%f,%f*00\n$GBMAG,%f,%f,%f*00\n$GBTMP,%f*00\n$GBTMR,%d*00\n$GBANG,%f,%f,%f*00\n",
//			imu->GetGyro()->GetValue()->X, imu->GetGyro()->GetValue()->Y, imu->GetGyro()->GetValue()->Z,
//			imu->GetAccel()->GetValue()->X, imu->GetAccel()->GetValue()->Y, imu->GetAccel()->GetValue()->Z,
//			imu->GetMag()->GetValue()->X, imu->GetMag()->GetValue()->Y, imu->GetMag()->GetValue()->Z,
//			imu->GetTemperature(), imu->GetTime(),
//			imu->GetEkf()->GetAngles()->roll, imu->GetEkf()->GetAngles()->pitch, imu->GetEkf()->GetAngles()->yaw);
//	size = strlen(buffer);
}

void Nmea::GenerateAccel(Sensor* sensor) {
	memset(buffer, 0, sizeof(buffer));
	sprintf(buffer, "$GBACC,%f,%f,%f*00\n", sensor->GetValue()->X,
			sensor->GetValue()->Y, sensor->GetValue()->Z);
	size = strlen(buffer);
}

void Nmea::GenerateGyro(Sensor* sensor) {
	memset(buffer, 0, sizeof(buffer));
	sprintf(buffer, "$GBGYR,%f,%f,%f*00\n", sensor->GetValue()->X,
			sensor->GetValue()->Y, sensor->GetValue()->Z);
	size = strlen(buffer);
}

void Nmea::GenerateMag(Sensor* sensor) {
	memset(buffer, 0, sizeof(buffer));
	sprintf(buffer, "$GBMAG,%f,%f,%f*00\n", sensor->GetValue()->X,
			sensor->GetValue()->Y, sensor->GetValue()->Z);
	size = strlen(buffer);
}

void Nmea::GenerateAngles(Angles* angles) {
	memset(buffer, 0, sizeof(buffer));
	sprintf(buffer, "$GBANG,%f,%f,%f*00\n", angles->roll, angles->pitch,
			angles->yaw);
	size = strlen(buffer);
}

void Nmea::GenerateKVAND(Angles* angles) {
	memset(buffer, 0, sizeof(buffer));
	uint8_t checkSum = 0;
	sprintf(buffer, "$cam_theta,%f,%f,%f*", angles->roll, angles->pitch,
			angles->yaw);
	size = strlen(buffer);
	for (uint8_t i = 1; i < size - 1; i++)
		checkSum ^= buffer[i];
	sprintf(buffer + size, "%x\n", checkSum);
	size = strlen(buffer);
}

void Nmea::GenerateKVANDWithoutCRC(Angles* angles) {
	memset(buffer, 0, sizeof(buffer));
	sprintf(buffer, "$cam_theta,%f,%f,%f\n", angles->roll, angles->pitch,
			angles->yaw);
	size = strlen(buffer);
}
