/**
 * @file photo.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Апрель 2015 года
*/

#ifndef INCLUDE_PHOTO_H_
#define INCLUDE_PHOTO_H_

#include "config.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "gpio.h"
#include "delay.h"

///@brief Управление режимами работы фотоаппарата
namespace PhotoName {

///@brief Состояние кнопки
typedef enum
{
	PhotoButton_Off = 0,	///< Кнопка отпущенна
	PhotoButton_On = 1	///< Кнопка нажата
} PhotoButton_TypeDef;



///@brief Управление режимами работы фотоаппарата
class Photo
{
	uint8_t zoomValue;	///< Значение кратности увеличения
	uint64_t zoomTime;	///< Время нажатия кнопки изменения кратности увеличения
	uint32_t zoomDelay;	///< Временная задержка для удержания кнопки изменения кратности увеличения
	bool zoomFlag;		///< Флаг нажатия кнопки изменения кратности увеличения

	uint64_t shotTime;	///< Время нажатия кнопки сделать снимок
	bool shotFlag;		///< Флаг нажатия кнопки сделать снимок

	uint64_t burstTime;	///< Время выполнения снимка в режиме серийной съемки
	uint32_t burstDelay;	///< Время задержки между снимками в серийной съемке
	bool burstFlag;		///< Флаг включения режима серийной съемки

	uint64_t shotTestTime;	///< Время выполнения тестового снимка
	bool shotTestFlag;	///< Флаг выполнения тестового снимка

	bool focusFlag;		///< Флаг нажатия кнопки фокусировки

	bool videoNotUsed;	///< Флаг использования фотоаппарата
	uint64_t sleepTime;	///< Время включения таймера для выключения фотоаппатара при неиспользовании
	uint32_t sleepDelay;	///< Задержка для выключения фотоаппарата при неиспользовании
	
	uint64_t initBatteryTime;	///< Время включения режима инициализации аккумулятора
	bool initBatteryFlag;		///< Флаг включения инициализации аккумулятора

	uint64_t initTime;	///< Время включения инициализации при включении фотоаппарата
	bool initFlag;		///< Флаг инициализации при включении фотоаппатара

	GpioName::Gpio* pinPower;	///< Вывод управления кнопкой питания фотоаппарата
	GpioName::Gpio* pinFocus;	///< Вывод управления кнопкой фокусировки
	GpioName::Gpio* pinShot;	///< Вывод управления кнопкой выполнением снимков
	GpioName::Gpio* pinZoomInc;	///< Вывод управляния кнопкой приближения 
	GpioName::Gpio* pinZoomDec;	///< Вывод управления кнопкой отдаления
	GpioName::Gpio* pinZoomIncFast;	///< Вывод управления кнопкой быстрого приближения
	GpioName::Gpio* pinZoomDecFast;	///< Вывод управления кнопкой быстрого отдаления
	GpioName::Gpio* pinUSBPower;	///< Вывод управления подачей питания на входной разъем USB фотоаппарата
	GpioName::Gpio* pinUSBControl;	///< Вывод управления замыкания между выводами D+ и D- USB, для включения режима зарядки

public:
	Photo(GpioName::GpioPort_TypeDef portPower, uint8_t pinPower,
			GpioName::GpioPort_TypeDef portFocus, uint8_t pinFocus,
			GpioName::GpioPort_TypeDef portShot, uint8_t pinShot,
			GpioName::GpioPort_TypeDef portZoomInc, uint8_t pinZoomInc,
			GpioName::GpioPort_TypeDef portZoomDec, uint8_t pinZoomDec,
			GpioName::GpioPort_TypeDef portZoomIncFast, uint8_t pinZoomIncFast,
			GpioName::GpioPort_TypeDef portZoomDecFast, uint8_t pinZoomDecFast,
			GpioName::GpioPort_TypeDef portUSBPower, uint8_t pinUSBPower,
			GpioName::GpioPort_TypeDef portUSBControl, uint8_t pinUSBControl);
	~Photo();
	void InitBattery();
	void Power(PhotoButton_TypeDef button);
	void Focus(PhotoButton_TypeDef button);
	void TestFoto();
	void TakeFoto();
	void Zoom(uint8_t zoomValue);
	void VideoNotUsed(bool value);
	void BurstMode(bool value, uint32_t delay);
	void Update(ConfigName::ConfigurationMUSV_TypeDef* configInput);

	inline void SetSleepDelaySec(uint8_t value) {sleepDelay = value * 1000000;}
};


}  // namespace PhotoName

#endif /* INCLUDE_PHOTO_H_ */
