/*
 * i2c.cpp
 *
 *  Created on: May 12, 2015
 *      Author: yura
 */

#include "../lib/include/i2c.h"

using namespace I2cName;

I2c::I2c(I2c_TypeDef i2c)
{
	state = I2cState_Init;
	time = 0;
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;


	I2C_InitTypeDef  I2C_InitStructure;
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = 0x26;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = 400000;

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	switch(i2c)
	{
	case I2c1:	//PB6 - SCL, PB7 - SDA
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		NVIC_InitStructure.NVIC_IRQChannel = I2C1_EV_IRQn;
		this->i2c = I2C1;
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_7;
		port = GPIOB;
		GPIO_PinAFConfig(port, GPIO_PinSource6, GPIO_AF_I2C1);
		GPIO_PinAFConfig(port, GPIO_PinSource7, GPIO_AF_I2C1);
		break;
	case I2c1_Remap:	//PB8 - SCL, PB9 - SDA
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		NVIC_InitStructure.NVIC_IRQChannel = I2C1_EV_IRQn;
		this->i2c = I2C1;
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8 | GPIO_Pin_9;
		port = GPIOB;
		GPIO_PinAFConfig(port, GPIO_PinSource8, GPIO_AF_I2C1);
		GPIO_PinAFConfig(port, GPIO_PinSource9, GPIO_AF_I2C1);
		break;
	case I2c2:	//PB10 - SCL, PB11 - SDA
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		NVIC_InitStructure.NVIC_IRQChannel = I2C2_EV_IRQn;
		this->i2c = I2C2;
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10 | GPIO_Pin_11;
		port = GPIOB;
		GPIO_PinAFConfig(port, GPIO_PinSource10, GPIO_AF_I2C2);
		GPIO_PinAFConfig(port, GPIO_PinSource11, GPIO_AF_I2C2);
		break;
	default:
		state = I2cState_Error;
		break;
	}

	if(state == I2cState_Init)
	{
		GPIO_Init(port, &GPIO_InitStructure);
		I2C_DeInit(this->i2c);
		I2C_Init(this->i2c, &I2C_InitStructure);

		I2C_Cmd(this->i2c, ENABLE);
//		I2C_AcknowledgeConfig(this->i2c, ENABLE);
//		NVIC_Init(&NVIC_InitStructure);
//		I2C_ITConfig(this->i2c, I2C_IT_EVT/* | I2C_IT_BUF*/, ENABLE);
	}
}

bool I2c::StartTransmission(uint8_t transmissionDirection, uint8_t slaveAddress)
{
	// На всякий случай ждем, пока шина осовободится
	time = Delay::GetMicrosecond();
	while (I2C_GetFlagStatus(i2c, I2C_FLAG_BUSY))
		if(Delay::GetMicrosecond() - time > 5000)
			return false;


	//I2C_ClearITPendingBit(I2Cx,I2C_IT_ARLO|I2C_IT_BERR|I2C_IT_TIMEOUT|I2C_IT_SMBALERT);

	// Генерируем старт
	I2C_GenerateSTART(i2c, ENABLE);
	// Ждем пока взлетит нужный флаг
	time = Delay::GetMicrosecond();
	while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_MODE_SELECT))
		if(Delay::GetMicrosecond() - time > 1000)
			return false;
	// Посылаем адрес подчиненному
	I2C_Send7bitAddress(i2c, slaveAddress, transmissionDirection);
	// А теперь у нас два варианта развития событий - в зависимости от выбранного направления обмена данными
	time = Delay::GetMicrosecond();
	if (transmissionDirection == I2C_Direction_Transmitter) {
		while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
			if(Delay::GetMicrosecond() - time > 1000)
			{
				I2C_GenerateSTOP(i2c, ENABLE);
				return false;
			};
	}
	if (transmissionDirection == I2C_Direction_Receiver) {
		while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
			if(Delay::GetMicrosecond() - time > 1000)
			{
				I2C_GenerateSTOP(i2c, ENABLE);
				return false;
			};
	}
}

// WriteRegAddr - адрес регистра по которому писать информацию
/*******************************************************************/
bool I2c::WriteData(uint8_t slaveAddressWrite,
		uint8_t WriteRegAddr, uint8_t Buffer) {
	//I2C_ClearITPendingBit(I2Cx,I2C_IT_ARLO|I2C_IT_BERR|I2C_IT_TIMEOUT|I2C_IT_SMBALERT);

	if(!StartTransmission(I2C_Direction_Transmitter, slaveAddressWrite))
		return false;

	I2C_SendData(i2c, WriteRegAddr);
	/* Test on EV8 and clear it */
	time = Delay::GetMicrosecond();
	while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
		if(Delay::GetMicrosecond() - time > 1000)
		{
			I2C_GenerateSTOP(i2c, ENABLE);
			return false;
		};

	/* Send the byte to be written */
	I2C_SendData(i2c, Buffer);

	/* Test on EV8 and clear it */
	time = Delay::GetMicrosecond();
	while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
		if(Delay::GetMicrosecond() - time > 1000)
		{
			I2C_GenerateSTOP(i2c, ENABLE);
			return false;
		};

	/* Send STOP condition */
	I2C_GenerateSTOP(i2c, ENABLE);
	return true;
}



uint8_t I2c::ReadByte(uint8_t slaveAddress, uint8_t ReadRegAddr)
{
	uint8_t data = 0;
	ReadData(slaveAddress, slaveAddress, ReadRegAddr, &data, 1);
	return data;
}


/*******************************************************************/
// Read Addr - адрес регистра по которому считывается информация
// slaveAddress - адрес устройства
bool I2c::ReadData(uint8_t slaveAddressWrite,
		uint8_t slaveAddressRead, uint8_t ReadRegAddr, u8* pBuffer,
		uint16_t NumByteToRead) {
	//I2C_ClearITPendingBit(I2Cx,I2C_IT_ARLO|I2C_IT_BERR|I2C_IT_TIMEOUT|I2C_IT_SMBALERT);

	if(!StartTransmission(I2C_Direction_Transmitter, slaveAddressWrite))	// Здесь slaveAddress - адрес для write
		return false;

	I2C_SendData(i2c, ReadRegAddr);
	time = Delay::GetMicrosecond();
	while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
		if(Delay::GetMicrosecond() - time > 1000)
		{
			I2C_GenerateSTOP(i2c, ENABLE);
			return false;
		};

	I2C_GenerateSTART(i2c, ENABLE);

	time = Delay::GetMicrosecond();
	while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_MODE_SELECT))
		if(Delay::GetMicrosecond() - time > 1000)
		{
			I2C_GenerateSTOP(i2c, ENABLE);
			return false;
		};

	I2C_Send7bitAddress(i2c, slaveAddressRead, I2C_Direction_Receiver);

	time = Delay::GetMicrosecond();
	while (!I2C_CheckEvent(i2c, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
		if(Delay::GetMicrosecond() - time > 1000)
		{
			I2C_GenerateSTOP(i2c, ENABLE);
			return false;
		};

	//I2C_StartTransmission(I2Cx, I2C_Direction_Receiver, slaveAddressRead); // Здесь slaveAddress - адрес для read

	time = Delay::GetMicrosecond();
	while (NumByteToRead) {
		if(Delay::GetMicrosecond() - time > 1000)
		{
			I2C_GenerateSTOP(i2c, ENABLE);
			I2C_AcknowledgeConfig(i2c, ENABLE);
			return false;
		}

		if (NumByteToRead == 1) {
			I2C_AcknowledgeConfig(i2c, DISABLE);
			I2C_GenerateSTOP(i2c, ENABLE);
		}
		if (I2C_CheckEvent(i2c, I2C_EVENT_MASTER_BYTE_RECEIVED)) {
			*pBuffer = I2C_ReceiveData(i2c);
			pBuffer++;
			NumByteToRead--;
			time = Delay::GetMicrosecond();
		}
	}
	I2C_AcknowledgeConfig(i2c, ENABLE);
	return true;
}

void I2c::Reset() {
	  I2C_ITConfig(i2c, I2C_IT_EVT|I2C_IT_BUF|I2C_IT_ERR,DISABLE);
	  I2C_SoftwareResetCmd(i2c,ENABLE);
	  (void)(i2c->SR1); // Clear ADDR Register
	  (void)(i2c->SR2);
	  (void)(i2c->DR);
	  I2C_SoftwareResetCmd(i2c,DISABLE);
}

I2cState_TypeDef I2c::GetState()
{
	return state;
}
