/**
 * @file motor.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Май 2015 года
*/

#ifndef INCLUDE_MOTOR_H_
#define INCLUDE_MOTOR_H_

#include "pwm.h"
#include "encoder.h"

///@brief Управление двигателем
namespace MotorName {

///@brief Направление вращения двигателя
enum MotorDirection_TypeDef
{
	MotorDirection_Forward = 1,	///< Вращение двигателя вперед
	MotorDirection_Revers = -1,	///< Вращение двигателя назад
};

///@brief Состояние двигателя
enum MotorState_TypeDef
{
	MotorState_Stop = 0,	///< Двигатель выключен
	MotorState_Run = 1,	///< Двигатель запущен
};

///@brief Управление двигателем
class Motor
{
	//Pwm section
	PwmName::Pwm* pwm;			///< Указатель на регистры используемого ШИМ генератора
	EncoderName::Encoder* encoder;		///< Указатель на класс обработки энкодера
	uint16_t period;			///< Период ШИМ импульсов
	float koefSpeed;			///< 
	MotorDirection_TypeDef direction;	///< Направление вращения
	MotorState_TypeDef state;		///< Состояние

	//PID section
	float Kp;			///< Пропорциональный коэффициент
	float scopeKp;			///< Ограничение пропорциональной составляющей
	float Ki;			///< Интегральный коэффициент
	float scopeKi;			///< Ограничение интегральной составляюзей
	float Kd;			///< Дифференциальный коэффициент
	float scopeKd;			///< Ограничение дифференциальной составляющей
	float L;			///< Ограничение результирующего сигнала
	float integrator;		///< Сумматор для накопления ошибки
	float deriv;			///< 
	float OutPID;			///< Выходной сигнал ПИД регулятора
	float errorAngleCurrent;	///< Текущая угловая ошибка
	float errorAnglePrevious;	///< Предыдущая угловая ошибка
	float time;			///< Период измерений

	//Регулятор для сглаживания углов управления
	float angleKp;			///< Пропорциональный коэффициент для задающих углов
	float finishAngle;		///< Выходной задающий угол

	//current section
	float current;			///< Текущий ток потребления двигателя
	float koefADCtoCurrentConvert;	///< Коэффициент перевода значения АЦП в значение тока
	uint16_t* currentADC;		///< Указатель на текущий использованный АЦП 


public:
	Motor(PwmName::Pwm_TypeDef pwm, uint16_t period, EncoderName::Encoder_TypeDef encoder);
	~Motor();
	void SetConvertKoef(float koef);
	float GetConvertKoef();
	void SetCurrentADC(uint16_t* current);
	void SetSpeed(float speed);
	void UpdatePID(float currentAngle, float finishAngle);
	void UpdateInputAnglesRegulatorP(float finishAngle);
	inline void UpdateEncoder() {encoder->Update();}
	float TrimSignalToScope ( float Signal, float Scope );

	inline void SetPIDKp(float value) {Kp = value;}
	inline void SetPIDScopeKp(float value) {scopeKp = value;}
	inline void SetPIDKi(float value) {Ki = value;}
	inline void SetPIDScopeKi(float value) {scopeKi = value;}
	inline void SetPIDKd(float value) {Kd = value;}
	inline void SetPIDScopeKd(float value) {scopeKd = value;}
	inline void SetPIDL(float value) {L = value;}
	inline float GetPIDKp() {return Kp;}
	inline float GetPIDScopeKp() {return scopeKp;}
	inline float GetPIDKi() {return Ki;}
	inline float GetPIDScopeKd() {return scopeKd;}
	inline float GetPIDKd() {return Kd;}
	inline float GetPIDScopeKi() {return scopeKi;}
	inline float GetPIDL() {return L;}

	inline float GetAngle() {return encoder->GetAngle();}
	inline float GetSpeed() {return encoder->GetSpeed();}

	inline MotorState_TypeDef GetState() {return state;}
	inline void SetState(MotorState_TypeDef value) {state = value;}

	inline void SetDirection(MotorDirection_TypeDef direction) {this->direction = direction;}
	inline void SetTime(float time) {this->time = time;}
};

}  // namespace MotorName


#endif /* INCLUDE_MOTOR_H_ */
