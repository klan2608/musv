/*
 * pwm.h
 *
 *  Created on: 22 апр. 2015 г.
 *      Author: klan
 */

#ifndef INCLUDE_PWM_H_
#define INCLUDE_PWM_H_


#include "stm32f4xx_gpio.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_rcc.h"

namespace PwmName {

enum PwmState_TypeDef
{
	PwmState_Off = 0,
	PwmState_On,
	PwmState_Init,
	PwmState_Deinit,
	PwmState_Error
};

enum PwmDiraction_TypeDef
{
	PwmDiraction_Forward = 0,
	PwmDiraction_Reverse = 1
};

enum Pwm_TypeDef
{
	Pwm_2,
	Pwm_3,
	Pwm_5,
	Pwm_8
};

class Pwm
{
	PwmState_TypeDef state;
	TIM_TypeDef* timer;
	GPIO_TypeDef* port;
public:
	Pwm(Pwm_TypeDef pwm, uint16_t period);
	void SetCompare(uint16_t channel_3, uint16_t channel_4);
};

}  // namespace PwmName


#endif /* INCLUDE_PWM_H_ */
