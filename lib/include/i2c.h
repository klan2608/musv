/**
 * @file gpio.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Май 2015 года
*/

#ifndef INCLUDE_I2C_H_
#define INCLUDE_I2C_H_

#include "stm32f4xx_i2c.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "delay.h"

using namespace DelayName;

///@brief Настройка и управление интерфейса I2C
namespace I2cName {
///@brief Состояние интерфейса I2C
enum I2cState_TypeDef : uint8_t
{
	I2cState_Off = 0,	///< Интерфейс выключен
	I2cState_On,		///< Интерфейс включен
	I2cState_Init,		///< Интерфейс в режиме настройки
	I2cState_Deinit,	///< Интерфейс сброшен в начальное состояние
	I2cState_Error		///< Ошибка при работе с интерфейсом
};
///@brief Номер интерфейса I2C
enum I2c_TypeDef
{
	I2c1,		///< I2C 1
	I2c1_Remap,	///< I2C 1 с переназначенными выводам
	I2c2		///< I2C 2
};
///@brief Настройка и управление интерфейса I2C
class I2c
{
	I2cState_TypeDef state;	///< Состояние интерфейса
	I2C_TypeDef* i2c;	///< Указатель на регистры интерфейса
	GPIO_TypeDef* port;	///< Указатель на порт выводов
	uint64_t time;		///< Время начала выполнения операций
public:
	I2c(I2c_TypeDef i2c);
	~I2c();
	bool ReadData(uint8_t slaveAddressWrite,
			uint8_t slaveAddressRead, uint8_t ReadRegAddr, u8* pBuffer,
			uint16_t NumByteToRead);
	uint8_t ReadByte(uint8_t slaveAddress, uint8_t ReadRegAddr);
	bool WriteData(uint8_t slaveAddressWrite,
			uint8_t WriteRegAddr, uint8_t Buffer);
	bool StartTransmission(uint8_t transmissionDirection, uint8_t slaveAddress);
	void Reset();
	I2cState_TypeDef GetState();
};


}  // namespace I2cName



#endif /* INCLUDE_I2C_H_ */
