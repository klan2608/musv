/**
 * @file nmea.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Май 2015 года
*/

#ifndef INCLUDE_NMEA_H_
#define INCLUDE_NMEA_H_

#include "imu.h"

///@brief Кодировка сообщений по формату сообщений NMEA
namespace NMEAName {

///@brief Кодировка сообщений по формату сообщений NMEA
class Nmea
{
	char buffer[200];		///< Массив для сообщений
	uint8_t size;			///< Размер полезных байт в массиве
	SensorName::Sensor* test;	
public:
	Nmea();
	void GenerateIMU(ImuName::Imu* imu);
	void GenerateTime(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minut, uint32_t second);
	void GenerateAccel(SensorName::Sensor* sensor);
	void GenerateGyro(SensorName::Sensor* sensor);
	void GenerateMag(SensorName::Sensor* sensor);
	void GenerateAngles(FilterName::Angles* angles);
	void GenerateKVAND(FilterName::Angles* angles);
	void GenerateKVANDWithoutCRC(FilterName::Angles* angles);
	void GenerateDebug(float value1, float value2, float value3, float value4, float value5);
	inline char* GetBuffer() {return buffer;}
	inline uint8_t GetSize() {return size;}
};

}  // namespace NMEAName



#endif /* INCLUDE_NMEA_H_ */
