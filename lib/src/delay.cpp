/**
 * @file delay.cpp
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Апрель 2015 года
*/


#include "../lib/include/delay.h"

using namespace DelayName;

volatile uint64_t Delay::workTimeSys;
volatile uint64_t Delay::delayTime;


/**
  * @brief  Инициализация системных тиков и установка начальных значений счетчиков
  *
  * Время одного тика счетчика составляет 100 микросекунд
  * @param  Нет
  * @retval Нет
  */
void Delay::Init()
{
	delayTime = 0;
	workTimeSys = 0;
	SysTick_Config(SystemCoreClock/10000);
}
/**
  * @brief Установка временной задержки
  * @param  delay значение задержки в микросекундах
  * @retval Нет
  */
void Delay::Wait(uint32_t delay)
{
	if(delay > 0)
	{
		delayTime = GetMicrosecond() + delay;
		while((int64_t) (delayTime - GetMicrosecond()) > 0)
			;
		delayTime = 0;
	}
}

/**
  * @brief  Увеличение значения текущего времени на 1
  * @param  Нет
  * @retval Нет
  */
void Delay::Tick()
{
	workTimeSys++;
}
/**
  * @brief  Получение текущего времени в миллисекундах
  * @param  Нет
  * @retval Текущее время в миллисекундах
  */
uint32_t Delay::GetMillesecond()
{
	return (uint32_t) workTimeSys / 10;
}
/**
  * @brief  Получение текущего времени в микросекундах
  * @param  Нет
  * @retval Текущее время в микросекундах
  */
uint64_t Delay::GetMicrosecond()
{
		return (uint64_t) workTimeSys * 100;
}

/**
  * @brief  Обработчик прерывания системного таймера
  * @param  Нет
  * @retval Нет
  */
extern "C" void
SysTick_Handler(void)
{
	Delay::Tick();
}
