/**
 * @file tvcamera.cpp
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Июнь 2015 года
*/

#include "../lib/include/tvcamera.h"

using namespace TVCameraName;
using namespace UsartName;

/**
  * @brief  Конструктор класса тв камеры
  * @param  usart - указатель на интерфейс USART
  * @retval Нет
  */
TVCamera::TVCamera(Usart* usart)
{
	usartCamera = usart;
}

/** @brief  Начальная настройка
  * @param  Нет
  * @retval Нет
  */
void TVCamera::Init()
{
	//Настройка камеры в режим SD видео (PAL)
	uint8_t Message[] = {0x81, 0x01, 0x04, 0x24, 0x00, 0x00, 0x00, 0xFF};
	Message[4] = 0x72;
	Message[5] = (0x0D >> 4)&0x0F;
	Message[6] = 0x0D & 0x0F;
	usartCamera->SendData(Message, sizeof(Message));
}


/** @brief  Установка кратности увеличения
  * @param  zoom - кратность увеличения
  * @retval Нет
  */
void TVCamera::SetZoom(uint8_t zoom)
{
	uint8_t Message[] = {0x81, 0x01, 0x04, 0x47, 0x00, 0x00, 0x00, 0x00, 0xFF};
	if (zoom > 20)
		zoom = 20;
	unsigned int zoomCamera= (0x00004000*(long)zoom)/20;   //0x4000 работает только в оптическом увеличении x20
	if(!zoomCamera)zoomCamera=1;
	Message[4]=(zoomCamera>>12)&0x0F;
	Message[5]=(zoomCamera>>8)&0x0F;
	Message[6]=(zoomCamera>>4)&0x0F;
	Message[7]=(zoomCamera)&0x0F;
	usartCamera->SendData(Message, sizeof(Message));
}

/** @brief  Включение камеры
  * @param  power - режим питания (true - включена, false - выключена)
  * @retval Нет
  */
void TVCamera::Power(bool power)
{
	uint8_t Message[] = {0x81, 0x01, 0x04, 0x00, 0x00, 0xFF};
	if(power)
		Message[4] = 0x02;
	else
		Message[4] = 0x03;
	usartCamera->SendData(Message, sizeof(Message));
}
