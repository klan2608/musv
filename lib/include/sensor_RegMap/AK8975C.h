#ifndef _AK8975C_H_
#define _AK8975C_H_

namespace SensorRegMapName {

#define AK8975C_ADDRESS   			0x0C // this device only has one address

//REGISTER
#define AK8975C_ID					0x00
#define AK8975C_INFO				0x01
#define AK8975C_STATUS1				0x02
#define AK8975C_MAG_XOUT_L			0x03
#define AK8975C_MAG_XOUT_H			0x04
#define AK8975C_MAG_YOUT_L			0x05
#define AK8975C_MAG_YOUT_H			0x06
#define AK8975C_MAG_ZOUT_L			0x07
#define AK8975C_MAG_ZOUT_H			0x08
#define AK8975C_STATUS2				0x09
#define AK8975C_CONTROL				0x0A
#define AK8975C_SELF_TEST			0x0C
#define AK8975C_I2C					0x0F
#define AK8975C_X_SENSITIVITY		0x10
#define AK8975C_Y_SENSITIVITY		0x11
#define AK8975C_Z_SENSITIVITY		0x12

//Device ID
#define AK8975C_ID_VALUE			0x48

//Status 1
#define AK8975C_STATUS1_READY			0x01

//Status 2
#define AK8975C_STATUS2_ERROR			0x04
#define AK8975C_STATUS2_HOFL			0x08

//Control
#define AK8975C_CONTROL_OFF				0x00
#define AK8975C_CONTROL_SINGLE			0x01
#define AK8975C_CONTROL_SELF_TEST		0x08
#define AK8975C_CONTROL_FUSE_ROM		0x0F

//Self-test control
#define AK8975C_SELF_TEST_NORMAL		0x00
#define AK8975C_SELF_TEST_MAGNETIC		0x40

//I2C disable
#define AK8975C_I2C_DISABLE				0x1B

}  // namespace SensorRegMapName


#endif
