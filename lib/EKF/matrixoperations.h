#ifndef MATRIXOPERATIONS_H
#define MATRIXOPERATIONS_H
#include <stdint.h>
#include <string.h>

//#include <iostream>
//using namespace std;
namespace MatrixOperations
{

    template<class T> static void addSquareMatrixes( T* left, T* right, T* result, uint32_t size )
    {
        //memset(result,0,size*size*sizeof(T));
		static uint32_t i;

		for ( i = 0; i < size * size; ++i ) {
			result[i] = left[i] + right[i];
        }
    }
    
    template<class T> static void subtractSquareMatrixes( T* left, T* right, T* result, uint32_t size )
    {
        //memset(result,0,size*size*sizeof(T));
		static uint32_t i;

		for ( i = 0; i < size * size; ++i ) {
			result[i] = left[i] - right[i];
        }
    }

	// Not used ? not optimized
    template<class T> static void addSquareMatrixesWithMultLeft( T* left, T leftMult,
            T* right,
            T* result, uint32_t size )
    {
        static uint32_t i, j;
        static uint32_t __muls_temp_ISize;
        
		for ( i = 0; i < size; ++i ) {
            __muls_temp_ISize = i * size;
            
			for ( j = 0; j < size; ++j ) {
                result[j + __muls_temp_ISize] = leftMult * left[j + __muls_temp_ISize] + right[j + __muls_temp_ISize];
            }
        }
    }
	// Not used ? not optimized
    template<class T> static void addSquareMatrixesWithMultBoth( T* left, T leftMult,
            T* right, T rightMult,
            T* result, uint32_t size )
    {
        static uint32_t i, j;
        static uint32_t __muls_temp_ISize;
        
		for ( i = 0; i < size; ++i ) {
            __muls_temp_ISize = i * size;
            
			for ( j = 0; j < size; ++j ) {
                result[j + __muls_temp_ISize] = leftMult * left[j + __muls_temp_ISize] + rightMult * right[j + __muls_temp_ISize];
            }
        }
    }
    
	template<class T, uint32_t size, uint32_t ByteSize> static void multSquareMatrixes( T* left, T* right, T* result )
    {
		memset( result, 0, ByteSize );
        static uint32_t i, j, k;
        static uint32_t __muls_temp_ISize;
        
		for ( i = 0; i < size; ++i ) {
            __muls_temp_ISize = i * size;

			for ( j = 0; j < size; ++j ) {
				static uint32_t cell_pos;
				cell_pos = j + __muls_temp_ISize;

				for ( k = 0; k < size; ++k ) {
					result[cell_pos] += ( left[k + __muls_temp_ISize] * right[j + k * size] );
                }
            }
        }
	}

	template<class T, uint32_t size, uint32_t ByteSize> static void multSquareMatrixesLeftBottomCornerZero( T* left, T* right, T* result )
	{
		//static_assert(size%2==0,"size must be even!");
		//static_assert(ByteSize%4==0,"byte size %4 must == 0!");
		static T bufAA[size * size];
		static T bufAB[size * size];
		static T bufBC[size * size];
		static T bufCC[size * size];
		static uint32_t i, j, k;
		memset( result, 0, ByteSize );
		memset( bufAA, 0, ByteSize >> 2 );
		memset( bufAB, 0, ByteSize >> 2 );
		memset( bufBC, 0, ByteSize >> 2 );
		memset( bufCC, 0, ByteSize >> 2 );

		static uint32_t halfSize = size >> 1;
		static uint32_t __muls_ISize;
		static uint32_t __muls_ISize_sum_HSize;
		static uint32_t __muls_IHSize;
		static uint32_t __sum_JHSize;
		static uint32_t __muls_HSizeSize = halfSize * size;
		static uint32_t __muls_HSizeSize_sum_HSize = __muls_HSizeSize + halfSize;
		static uint32_t __muls_HSizeSize_sum_HSize_sumJ;
		static uint32_t __muls_HSizeSize_sum_HSize_sumJ_sumISize;
		static uint32_t cell_pos;
		//AA
		for ( i = 0; i < halfSize; ++i ) {
			__muls_ISize = i * size;
			__muls_IHSize = i * halfSize;

			for ( j = 0; j < halfSize; ++j ) {
				cell_pos = j + __muls_IHSize;

				for ( k = 0; k < halfSize; ++k ) {
					bufAA[cell_pos] += ( left[k + __muls_ISize] * right[j + k * size] );
				}
			}
		}

		//AB
		for ( i = 0; i < halfSize; ++i ) {
			__muls_ISize = i * size;
			__muls_IHSize = i * halfSize;

			for ( j = 0; j < halfSize; ++j ) {
				__sum_JHSize = j + halfSize;
				cell_pos = j + __muls_IHSize;

				for ( k = 0; k < halfSize; ++k ) {
					bufAB[cell_pos] += ( left[k + __muls_ISize] * right[__sum_JHSize + k * size] );
				}
			}
		}

		//BC


		for ( i = 0; i < halfSize; ++i ) {
			__muls_ISize_sum_HSize = i * size + halfSize;
			__muls_IHSize = i * halfSize;

			for ( j = 0; j < halfSize; ++j ) {
				__muls_HSizeSize_sum_HSize_sumJ = j + __muls_HSizeSize_sum_HSize ;

				cell_pos = j + __muls_IHSize;

				for ( k = 0; k < halfSize; ++k ) {

					bufBC[cell_pos] += ( left[k + __muls_ISize_sum_HSize] * right[__muls_HSizeSize_sum_HSize_sumJ + k * size] );
				}
			}
		}

		//CC

		for ( i = 0; i < halfSize; ++i ) {
			__muls_HSizeSize_sum_HSize_sumJ_sumISize = __muls_HSizeSize_sum_HSize + i * size;
			__muls_IHSize = i * halfSize;
			for ( j = 0; j < halfSize; ++j ) {
				__muls_HSizeSize_sum_HSize_sumJ = j + __muls_HSizeSize_sum_HSize ;
				cell_pos = j + __muls_IHSize;

				for ( k = 0; k < halfSize; ++k ) {
					bufCC[cell_pos] += ( left[k + __muls_HSizeSize_sum_HSize_sumJ_sumISize] * right[__muls_HSizeSize_sum_HSize_sumJ + k * size] );
				}
			}
		}

		// Copy AA
		for ( i = 0; i < halfSize; ++i ) {
			__muls_ISize = i * size;
			__muls_IHSize = i * halfSize;
			memcpy( result + __muls_ISize, bufAA + __muls_IHSize, halfSize * sizeof( T ) );
		}

		// Copy CC
		for ( i = 0; i < halfSize; ++i ) {
			__muls_ISize = i*size + __muls_HSizeSize_sum_HSize;
			__muls_IHSize = i * halfSize;
			memcpy( result + __muls_ISize, bufCC + __muls_IHSize, halfSize * sizeof( T ) );
		}

		// Add AB to BC
		for ( i = 0; i < halfSize * halfSize; ++i ) {
			bufBC[i] += bufAB[i];
		}

		// Copy sum
		for ( i = 0; i < halfSize; ++i ) {
			__muls_ISize =  i  * size + halfSize;
			__muls_IHSize = i * halfSize;
			memcpy( result + __muls_ISize, bufBC + __muls_IHSize, halfSize * sizeof( T ) );
		}

	}
    
    template<class T, uint32_t ByteSize> static void multSquareMatrixesTransposingRight( T* left, T* right, T* result, uint32_t size )
    {
        memset( result, 0, ByteSize );
        static uint32_t i, j, k;
        static uint32_t __muls_temp_ISize;
        
		for ( i = 0; i < size; ++i ) {
            __muls_temp_ISize = i * size;
            
			for ( j = 0; j < size; ++j ) {
				static uint32_t __muls_JSize;
				__muls_JSize = j * size;
				static uint32_t cell_pos;
				cell_pos = j + __muls_temp_ISize;

				for ( k = 0; k < size; ++k ) {
					result[cell_pos] += ( left[k + __muls_temp_ISize] * right[k + __muls_JSize] );
                }
            }
        }
    }
    
    template<class T, uint32_t ByteSize> static void multMatrixes( T* left, T* right, T* result,
            uint32_t lsize, uint32_t msize, uint32_t rsize )
    {
        memset( result, 0, ByteSize );
        static uint32_t i, j, k;
        static uint32_t __mul_temp_IRSize;
        static uint32_t __mul_temp_IMSize;
        
		for ( i = 0; i < lsize; ++i ) {
            __mul_temp_IRSize = i * rsize;
            __mul_temp_IMSize = i * msize;
            
			for ( j = 0; j < rsize; ++j ) {
				static uint32_t cell_pos;
				cell_pos = j + __mul_temp_IRSize;

				for ( k = 0; k < msize; ++k ) {
					result[cell_pos] += left[k + __mul_temp_IMSize] * right[j + k * rsize];
                }
            }
        }
    }
    
    
    template<class T, uint32_t ByteSize> static void multMatrixesRightTransponse( T* left, T* right, T* result,
            uint32_t lsize, uint32_t msize, uint32_t rsize )
    {
        memset( result, 0, ByteSize );
        static uint32_t __mul_temp_IMSize;
        static uint32_t __mul_temp_IRSize;
        static uint32_t __mul_temp_JMSize;
        static uint32_t i, j, k;
        
		for ( i = 0; i < lsize; ++i ) {
            __mul_temp_IMSize = i * msize;
            __mul_temp_IRSize = i * rsize;
            
			for ( j = 0; j < rsize; ++j ) {
                __mul_temp_JMSize = j * msize;
				static uint32_t cell_pos;
				cell_pos = j + __mul_temp_IRSize;
                
				for ( k = 0; k < msize; ++k ) {
					result[cell_pos] += left[k + __mul_temp_IMSize] * right[k + __mul_temp_JMSize];
                }
            }
        }
    }
    
    /*
    * копируем меньшую матрицу в большую, никакой проверки параметров, всё на вашей совести
    *
    *                                     destColumn                    destColumn+sourceSize
    *
    *                      [ ...             ...            ...               ...                    ... ]
    * destRow              [ ...          source[0,0]       ...        sorce[0,sourceSize-1]         ... ]
    *                      [ ...             ...            ...               ...                    ... ]
    * destRow+sourceSize   [ ...   source[sourceSize-1,0]   ...   source[sourceSize-1,sourceSize-1]  ... ]
    *                      [ ...             ...            ...               ...                    ... ]
    */
    
    template<class T> static void copySquareToPartOfSquare( T* destination, T* source,
            uint32_t destSize, uint32_t sourceSize,
            uint32_t destRow, uint32_t destColumn )
    {
        static uint32_t CopySize;
        static uint32_t i;
        CopySize = sourceSize * sizeof( T );
        
		for ( i = 0; i < sourceSize; ++i ) {
            memcpy( destination + destSize * ( destRow + i ) + destColumn, source + sourceSize * i, CopySize );
        }
    }
    
    template<class T> static void copyTranposingSquareToPartOfSquare( T* destination, T* source,
            uint32_t destSize, uint32_t sourceSize,
            uint32_t destRow, uint32_t destColumn )
    {
        static uint32_t i, j;
        static uint32_t __cIT_D_ISize;
        
		for ( i = 0; i < sourceSize; ++i ) {
            __cIT_D_ISize = destSize * ( destRow + i ) + destColumn;
            
			for ( j = 0; j < sourceSize; ++j ) {
                destination[__cIT_D_ISize + j] = source[i + j * sourceSize];
            }
        }
    }
    
    /*
    * копируем часть большей матрицы в меньшую, никакой проверки параметров, всё на вашей совести
    * параметры как для предыдущей функции
    */
    template<class T> static void copyPartOfSquareToSquare( T* destination, T* source,
            uint32_t destSize, uint32_t sourceSize,
            uint32_t sourceRow, uint32_t sourceColumn )
    {
        static uint32_t i;
        static uint32_t CopySize;
        CopySize = destSize * sizeof( T );
        
		for ( i = 0; i < destSize; ++i ) {
            memcpy( destination + destSize * i, source + sourceSize * ( i + sourceRow ) + sourceColumn, CopySize );
        }
    }
    
    template<class T> static void copyTransposingPartOfSquareToSquare( T* destination, T* source,
            uint32_t destSize, uint32_t sourceSize,
            uint32_t sourceRow, uint32_t sourceColumn )
    {
        static uint32_t i, j;
        static uint32_t __cFT_D_ISize ;
        
		for ( i = 0; i < destSize; ++i ) {
            __cFT_D_ISize = sourceSize * ( sourceRow + i ) + sourceColumn;
            
			for ( j = 0; j < destSize; ++j ) {
                destination[i + j * destSize] = source[j + __cFT_D_ISize];
            }
        }
    }
    
    /*
    * домнажаем часть матрицы на константу
    *
    */
    
    template<class T> static void multPart( T* matrix, uint32_t size,
                                            uint32_t rowCount, uint32_t colCount,
                                            uint32_t BeginRow, uint32_t BeginColumn,
                                            T mul )
    {
        static uint32_t i, j;
        static uint32_t _mul_part_ISize;
        
		for ( i = 0; i < rowCount; ++i ) {
            _mul_part_ISize = BeginColumn + ( BeginRow + i ) * size;
            
			for ( j = 0; j < colCount; ++j ) {
                matrix[j + _mul_part_ISize] *= mul;
            }
        }
    }
    
    template<class T> static void multSquareOnScalar( T* matrix, uint32_t size,
            T* result, T mul )
    {
        static uint32_t i, j;
        static uint32_t _mul_part_ISize;
        
		for ( i = 0; i < size; ++i ) {
            _mul_part_ISize = i * size;
            
			for ( j = 0; j < size; ++j ) {
                result[j + _mul_part_ISize] = matrix[j + _mul_part_ISize] * mul;
            }
        }
    }
    
    /**
    * итак я буду использовать метод Гаусса,
    */
    
    template<class T, uint32_t size, uint32_t ByteSize> static void invSquareMatrix( T* source,  T* result/*, uint32_t size*/ )
    {
        static uint32_t __inv_temp_ISize;
        static uint32_t __inv_temp_JSize;
        static T __tempMultiplier;
        static T tempMatrix[size * size];
        static uint32_t i, j, k;
        memcpy( tempMatrix, source, ByteSize );
        memset( result, 0, ByteSize );
        
		for ( i = 0; i < size; ++i ) {
            result[i * ( size + 1 )] = 1;
        }
        

		for ( i = 0; i < size; ++i ) {
            __inv_temp_ISize = i * size;
            multPart<T>( result,      size , 1, size,  i , 0 , 1.0 / tempMatrix[i + __inv_temp_ISize] );
            multPart<T>( tempMatrix,  size , 1, size - i,  i , i , 1.0 / tempMatrix[i + __inv_temp_ISize] );
            
			for ( j = 0; j < i; ++j ) {
                __inv_temp_JSize = j * size;
                __tempMultiplier = ( tempMatrix[i + __inv_temp_JSize] ); // tempMatrix[i+__inv_temp_ISize] == 1 now
                
				for ( k = 0; k < size; ++k ) {
                    result    [k + __inv_temp_JSize] -= __tempMultiplier * result    [k + __inv_temp_ISize];
                    tempMatrix[k + __inv_temp_JSize] -= __tempMultiplier * tempMatrix[k + __inv_temp_ISize];
                }
            }
            
			for ( j = i + 1; j < size; ++j ) {
                __inv_temp_JSize = j * size;
                __tempMultiplier = ( tempMatrix[i + __inv_temp_JSize] ); // tempMatrix[i+__inv_temp_ISize] == 1 now
                
				for ( k = 0; k < size; ++k ) {
                    result    [k + __inv_temp_JSize] -= __tempMultiplier * result    [k + __inv_temp_ISize];
                    tempMatrix[k + __inv_temp_JSize] -= __tempMultiplier * tempMatrix[k + __inv_temp_ISize];
                }
            }
        }
    }
    
    
    template<class T, uint32_t size, uint32_t ByteSize> static void mldivideA( T* left,  T* right )
    {
        static uint32_t __inv_temp_ISize;
        static uint32_t __inv_temp_JSize;
        static T __tempMultiplier;
        static T tempMatrix[size * size];
        static uint32_t i, j, k;
        memcpy( tempMatrix, left, ByteSize );
        
		for ( i = 0; i < size; ++i ) {
            __inv_temp_ISize = i * size;
            multPart<T>( right,      size , 1, size,  i , 0 , 1.0 / tempMatrix[i + __inv_temp_ISize] );
            multPart<T>( tempMatrix,  size , 1, size - i,  i , i , 1.0 / tempMatrix[i + __inv_temp_ISize] );
            
			for ( j = 0; j < i; ++j ) {
                __inv_temp_JSize = j * size;
                __tempMultiplier = ( tempMatrix[i + __inv_temp_JSize] ); // tempMatrix[i+__inv_temp_ISize] == 1 now
                
				for ( k = i; k < size; ++k ) {
                    right    [k + __inv_temp_JSize] -= __tempMultiplier * right    [k + __inv_temp_ISize];
                    tempMatrix[k + __inv_temp_JSize] -= __tempMultiplier * tempMatrix[k + __inv_temp_ISize];
                }
            }
            
			for ( j = i + 1; j < size; ++j ) {
                __inv_temp_JSize = j * size;
                __tempMultiplier = ( tempMatrix[i + __inv_temp_JSize] ); // tempMatrix[i+__inv_temp_ISize] == 1 now
                
				for ( k = i; k < size; ++k ) {
                    right    [k + __inv_temp_JSize] -= __tempMultiplier * right    [k + __inv_temp_ISize];
                    tempMatrix[k + __inv_temp_JSize] -= __tempMultiplier * tempMatrix[k + __inv_temp_ISize];
                }
            }
        }
    }
    
}

#endif // MATRIXOPERATIONS_H
