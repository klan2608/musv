/**
 * @file imu.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Май 2015 года
*/

#ifndef INCLUDE_IMU_H_
#define INCLUDE_IMU_H_

#include "sensor.h"
#include "eeprom.h"
#include "filter.h"
#include "i2c.h"
#include "gpio.h"
#include "sensor_RegMap/HMC8553L.h"
#include "sensor_RegMap/MPU9150.h"
#include "sensor_RegMap/MPUxxxx.h"
#include "sensor_RegMap/AK8975C.h"
#include "delay.h"
#include "string.h"
#include <stdio.h>
#include <string.h>
///@brief Обработка датчиков БИНС
namespace ImuName {
///@brief Состояние БИНС
enum ImuState_TypeDef:uint8_t
{
	ImuState_Error = 0,		///< Отказ БИНС
	ImuState_Correct = 1		///< БИНС работает исправно
};
///@brief Тип используемого магнитометра
enum ImuMag_TypeDef
{
	ImuMag_ExternalHMC,		///< Внешний HMC8553L
	ImuMag_Internal			///< Внутренный в MPU9150
};
///@brief Данные для калибровки магнитометра
struct ImuCalibration_TypeDef
{
	SensorName::Axis<float> max;		///< Максимальные значения по осям
	SensorName::Axis<float> min;		///< Минимальные значения по осям
	SensorName::Axis<float> bias;		///< Смещения по осям
	SensorName::Axis<float> scale;		///< Маштабирование по осям
};
///@brief Обработка датчиков БИНС
class Imu {
	ImuState_TypeDef state;			///< Состоние БИНС
	ImuMag_TypeDef magType;			///< Тип используемого магнитометра
	SensorName::Sensor* accel;		///< Акселерометр 3-оси
	SensorName::Sensor* gyro;		///< Гироскоп 3-оси
	SensorName::Axis<float>* gyroInit;		///< Значения начальных смещенний гироскопов
	SensorName::Sensor* mag;		///< Магнитометр 3-оси
	float temperature;			///< Температура MPU9150
	uint32_t time;				///< Период измерений
	uint64_t timeLast;			///< Время предыдущего измерения
	uint8_t address;			///< Адресс датчиков
	uint16_t iterator;			///< 
	FilterName::Filter ekf;			///< Фильтр для получения угловых положений в пространстве
	ImuCalibration_TypeDef calibData;	///< Данные калибровки магнитометра
	bool initialization;			///< Режим инициализации
	bool calibration;			///< Режим калибровки
	uint8_t eepromID;			///< Идентификационный номер для использования памяти Eeprom

	GpioName::Gpio* pinInterrupt;		///< Вывод готовности данных

public:
	I2cName::I2c* i2c;

	Imu(I2cName::I2c_TypeDef i2c, uint16_t frequency, ImuMag_TypeDef magType, uint8_t address = 0xD2);
	~Imu();
	void Init();
	void UpdateValue();
	void ResetAllData();
	ImuState_TypeDef CheckConection();
	void Handler();
	void ReadCalibrationDataFromMemory();
	ImuCalibration_TypeDef GetCalibData(){return calibData;}
	void SetCalibration(bool value);
	inline void ReserveMemoryEeprom() {eepromID = EepromName::Eeprom::ReserveMemory(6);}
	inline bool GetCalibration() {return calibration;}
	inline SensorName::Sensor* GetAccel() {return accel;}
	inline SensorName::Sensor* GetGyro() {return gyro;}
	inline SensorName::Sensor* GetMag() {return mag;}
	inline float GetTemperature() {return temperature;}
	inline int GetTime() {return time;}
	inline FilterName::Filter* GetEkf() {return &ekf;}
	inline FilterName::Angles* GetAngles() {return ekf.GetAngles();}
	inline FilterName::Angles* GetAnglesRaw() {return ekf.GetAnglesRaw();}
	inline ImuState_TypeDef GetState(){return state;}
};

}  // namespace ImuName

#endif /* INCLUDE_IMU_H_ */
