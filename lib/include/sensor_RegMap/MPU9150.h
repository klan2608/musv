#ifndef _MPU9150_H_
#define _MPU9150_H_

//MPU-60X0 Registers Addresses

namespace SensorRegMapName {

#define MPU9150_ADDRESS		0xD0

#define	SELF_TEST_X 		0x0D
#define	SELF_TEST_Y 		0x0E
#define	SELF_TEST_Z 		0x0F
#define	SELF_TEST_A 		0x10
#define	SMPLRT_DIV 			0x19
#define	CONFIG 				0x1A
#define	GYRO_CONFIG 		0x1B
#define	ACCEL_CONFIG 		0x1C
#define	MOT_THR 			0x1F
#define	FIFO_EN 			0x23
#define	I2C_MST_CTRL 		0x24
#define	I2C_SLV0_ADDR 		0x25
#define	I2C_SLV0_REG 		0x26
#define	I2C_SLV0_CTRL 		0x27
#define	I2C_SLV1_ADDR 		0x28
#define	I2C_SLV1_REG 		0x29
#define	I2C_SLV1_CTRL  		0x2A
#define	I2C_SLV2_ADDR 		0x2B
#define	I2C_SLV2_REG 		0x2C
#define	I2C_SLV2_CTRL 		0x2D
#define	I2C_SLV3_ADDR 		0x2E
#define	I2C_SLV3_REG 		0x2F
#define	I2C_SLV3_CTRL 		0x30
#define	I2C_SLV4_ADDR 		0x31
#define	I2C_SLV4_REG 		0x32
#define	I2C_SLV4_DO 		0x33
#define	I2C_SLV4_CTRL 		0x34
#define	I2C_SLV4_DI 		0x35
#define	I2C_MST_STATUS		0x36
#define	INT_PIN_CFG  		0x37
#define	INT_ENABLE 			0x38
#define	INT_STATUS  		0x3A
#define	ACCEL_XOUT_H 		0x3B
#define	ACCEL_XOUT_L 		0x3C
#define	ACCEL_YOUT_H 		0x3D
#define	ACCEL_YOUT_L 		0x3E
#define	ACCEL_ZOUT_H 		0x3F
#define	ACCEL_ZOUT_L 		0x40
#define	TEMP_OUT_H 			0x41
#define	TEMP_OUT_L 			0x42
#define	GYRO_XOUT_H 		0x43
#define	GYRO_XOUT_L 		0x44
#define	GYRO_YOUT_H 		0x45
#define	GYRO_YOUT_L 		0x46
#define	GYRO_ZOUT_H 		0x47
#define	GYRO_ZOUT_L 		0x48
#define	EXT_SENS_DATA_00	0x49
#define	EXT_SENS_DATA_01	0x4A
#define	EXT_SENS_DATA_02	0x4B
#define	EXT_SENS_DATA_03	0x4C
#define	EXT_SENS_DATA_04	0x4D
#define	EXT_SENS_DATA_05	0x4E
#define	EXT_SENS_DATA_06	0x4F
#define	EXT_SENS_DATA_07	0x50
#define	EXT_SENS_DATA_08	0x51
#define	EXT_SENS_DATA_09	0x52
#define	EXT_SENS_DATA_10	0x53
#define	EXT_SENS_DATA_11	0x54
#define	EXT_SENS_DATA_12	0x55
#define	EXT_SENS_DATA_13	0x56
#define	EXT_SENS_DATA_14	0x57
#define	EXT_SENS_DATA_15	0x58
#define	EXT_SENS_DATA_16	0x59
#define	EXT_SENS_DATA_17	0x5A
#define	EXT_SENS_DATA_18	0x5B
#define	EXT_SENS_DATA_19	0x5C
#define	EXT_SENS_DATA_20	0x5D
#define	EXT_SENS_DATA_21	0x5E
#define	EXT_SENS_DATA_22	0x5F
#define	EXT_SENS_DATA_23	0x60
#define	I2C_SLV0_DO 		0x63
#define	I2C_SLV1_DO 		0x64
#define	I2C_SLV2_DO 		0x65
#define	I2C_SLV3_DO 		0x66
#define	I2C_MST_DELAY_CTRL	0x67
#define	SIGNAL_PATH_RESET	0x68
#define	MOT_DETECT_CTRL		0x69
#define	USER_CTRL 			0x6A
#define	PWR_MGMT_1 			0x6B
#define	PWR_MGMT_2 			0x6C
#define	FIFO_COUNTH 		0x72
#define	FIFO_COUNTL  		0x73
#define	FIFO_R_W  			0x74
#define	WHO_AM_I 			0x75

//MPU-60X0 Configurations bits

//Sample rates macro definitions
#define SAMPLE_RATE_1kHz  	0
#define SAMPLE_RATE_500Hz 	1
#define SAMPLE_RATE_200Hz 	4
#define SAMPLE_RATE_100Hz 	9
#define SAMPLE_RATE_50Hz  	19
#define SAMPLE_RATE_25Hz  	39
#define SAMPLE_RATE_20Hz  	49
#define SAMPLE_RATE_10Hz  	99
#define SAMPLE_RATE_5Hz  	199
#define SAMPLE_RATE_1Hz  	999
#define SAMPLE_RATE_43Hz  	22
#define SAMPLE_RATE_40Hz  	24
#define SAMPLE_RATE_35Hz  	27
#define SAMPLE_RATE_33Hz  	29

//Bitdefs for CONFIG register
//FSYNC bitdefs
#define BIT_FSYNC_DIS 		0x00
#define BIT_TEMP_OUT_L 		0x08
#define BIT_GYRO_XOUT_L 	0x10
#define BIT_GYRO_YOUT_L 	0x18
#define BIT_GYRO_ZOUT_L 	0x20
#define BIT_ACCEL_XOUT_L 	0x28
#define BIT_ACCEL_YOUT_L 	0x30
#define BIT_ACCEL_ZOUT_L 	0x38
//DLPF bitdefs
#define BIT_DLPF_CFG_256Hz 	0x00
#define BIT_DLPF_CFG_188Hz 	0x01
#define BIT_DLPF_CFG_98Hz 	0x02
#define BIT_DLPF_CFG_42Hz 	0x03
#define BIT_DLPF_CFG_20Hz 	0x04
#define BIT_DLPF_CFG_10Hz 	0x05
#define BIT_DLPF_CFG_5Hz 	0x06
#define BIT_DLPF_CFG_Res 	0x07

//Bitdefs for GYRO_CONFIG register
//Gyro scale range bitdefs
#define BIT_FS_SEL_250DPS 	0x00
#define BIT_FS_SEL_500DPS 	0x08
#define BIT_FS_SEL_1000DPS 	0x10
#define BIT_FS_SEL_2000DPS 	0x18
//Self calibration bitdefs
#define BIT_XG_ST_EN 		0x80
#define BIT_YG_ST_EN 		0x40
#define BIT_ZG_ST_EN 		0x20

//Bitdefs for ACCEL_CONFIG register
//Gyro scale range bitdefs
#define BIT_AFS_SEL_2G 		0x00
#define BIT_AFS_SEL_4G 		0x08
#define BIT_AFS_SEL_8G 		0x10
#define BIT_AFS_SEL_16G 	0x18
//Self calibration bitdefs
#define BIT_XA_ST_EN 		0x80
#define BIT_YA_ST_EN 		0x40
#define BIT_ZA_ST_EN 		0x20

//Bitdefs for MOT_THR register

//Bitdefs for FIFO_EN register
#define BIT_TEMP_FIFO_EN 	0x80
#define BIT_XG_FIFO_EN 		0x40
#define BIT_YG_FIFO_EN 		0x20
#define BIT_ZG_FIFO_EN 		0x10
#define BIT_ACCEL_FIFO_EN 	0x08
#define BIT_SLV2_FIFO_EN 	0x04
#define BIT_SLV1_FIFO_EN 	0x02
#define BIT_SLV0_FIFO_EN 	0x01

//Bitdefs I2C_MST_CTRL
#define BIT_MULT_MST_EN 	0x80
#define BIT_WAIT_FOR_ES 	0x40
#define BIT_SLV3_FIFO_EN 	0x20
#define BIT_I2C_MST_P_NSR 	0x10
#define BIT_I2C_MST_CLK_348kHz 0x00
#define BIT_I2C_MST_CLK_333kHz 0x01
#define BIT_I2C_MST_CLK_320kHz 0x02
#define BIT_I2C_MST_CLK_308kHz 0x03
#define BIT_I2C_MST_CLK_296kHz 0x04
#define BIT_I2C_MST_CLK_286kHz 0x05
#define BIT_I2C_MST_CLK_276kHz 0x06
#define BIT_I2C_MST_CLK_267kHz 0x07
#define BIT_I2C_MST_CLK_258kHz 0x08
#define BIT_I2C_MST_CLK_500kHz 0x09
#define BIT_I2C_MST_CLK_471kHz 0x0A
#define BIT_I2C_MST_CLK_444kHz 0x0B
#define BIT_I2C_MST_CLK_421kHz 0x0C
#define BIT_I2C_MST_CLK_400kHz 0x0D
#define BIT_I2C_MST_CLK_381kHz 0x0E
#define BIT_I2C_MST_CLK_364kHz 0x0F

//Bitdefs I2C_SLV0_ADDR, I2C_SLV0_REG, I2C_SLV0_CTRL bitdefs
#define I2C_SLV0_R 			0x80
#define I2C_SLV0_W 			0x00
#define I2C_SLV0_EN 		0x80
#define I2C_SLV0_BYTE_SW 	0x40
#define I2C_SLV0_REG_DIS 	0x20
#define I2C_SLV0_GRP     	0x10

//Bitdefs I2C_SLV1_ADDR, I2C_SLV1_REG, I2C_SLV1_CTRL bitdefs
#define I2C_SLV1_R 			0x80
#define I2C_SLV1_W 			0x00
#define I2C_SLV1_EN 		0x80
#define I2C_SLV1_BYTE_SW 	0x40
#define I2C_SLV1_REG_DIS 	0x20
#define I2C_SLV1_GRP     	0x10

//Bitdefs I2C_SLV2_ADDR, I2C_SLV2_REG, I2C_SLV2_CTRL bitdefs
#define I2C_SLV2_R 			0x80
#define I2C_SLV2_W 			0x00
#define I2C_SLV2_EN 		0x80
#define I2C_SLV2_BYTE_SW 	0x40
#define I2C_SLV2_REG_DIS 	0x20
#define I2C_SLV2_GRP     	0x10

//Bitdefs I2C_SLV3_ADDR, I2C_SLV3_REG, I2C_SLV3_CTRL bitdefs
#define I2C_SLV3_R 			0x80
#define I2C_SLV3_W 			0x00
#define I2C_SLV3_EN 		0x80
#define I2C_SLV3_BYTE_SW 	0x40
#define I2C_SLV3_REG_DIS 	0x20
#define I2C_SLV3_GRP     	0x10

//Bitdefs 49 - 53 registers

//Bitdefs INT_PIN_CFG register
#define BIT_INT_LEVEL_LOW 	0x80
#define BIT_INT_OPEN 		0x40
#define BIT_INT_LATCN_INT_EN 0x20
#define BIT_INT_RD_CLEAR 	0x10
#define BIT_FSYNC_INT_LEVEL 0x08
#define BIT_FSYNC_INT_EN 	0x04
#define BIT_I2C_BYPASS_EN 	0x02

//Bitdefs INT_ENABLE register
#define BIT_MOT_EN 			0x40
#define BIT_FIFO_OFLOW_EN 	0x10
#define BIT_I2C_MST_INT_EN 	0x08
#define BIT_DATA_RDY_EN 	0x01

//Bitdefs I2C_MST_DELAY_CTRL register
#define BIT_DELAY_ES_SHADOW 0x80
#define BIT_I2C_SLV4_DLY_EN 0x10
#define BIT_I2C_SLV3_DLY_EN 0x08
#define BIT_I2C_SLV2_DLY_EN 0x04
#define BIT_I2C_SLV1_DLY_EN 0x02
#define BIT_I2C_SLV0_DLY_EN 0x01

//Bitdefs SIGNAL_PATH_RESET register
#define BIT_GYRO_RESET 		0x04
#define BIT_ACCEL_RESET 	0x02
#define BIT_TEMP_RESET 		0x01

//Bitdefs MOT_DETECT_CTRL register
#define ACCEL_ON_DELAY_1ms 	0x00
#define ACCEL_ON_DELAY_2ms 	0x10
#define ACCEL_ON_DELAY_3ms 	0x20
#define ACCEL_ON_DELAY_4ms 	0x30

//Bitdefs USER_CTRL register
#define BIT_FIFO_EN 		0x40
#define BIT_I2C_MST_EN 		0x20
#define BIT_I2C_I2C_IF_DIS 	0x10
#define BIT_FIFO_RESET 		0x04
#define BIT_I2C_MST_RESET 	0x02
#define BIT_SIG_COND_RESET 	0x01

//Bitdefs PWR_MGMT_1
#define BIT_DEVICE_RESET 			0x80
#define BIT_SLEEP 					0x40
#define BIT_CYCLE 					0x20
#define BIT_TEMP_DIS 				0x08
#define BIT_CLKSEL_INT8MHz 			0x00
#define BIT_CLKSEL_PLL_XGYRO 		0x01
#define BIT_CLKSEL_PLL_YGYRO 		0x02
#define BIT_CLKSEL_PLL_ZGYRO 		0x03
#define BIT_CLKSEL_PLL_EXT32kHz 	0x04
#define BIT_CLKSEL_PLL_EXT19MHz 	0x05
#define BIT_CLKSEL_STOP 			0x07

//Bidefs PWR_MGMT_2
// #define BIT_LP_WAKE_CTRL_1.25Hz	0x00
#define BIT_LP_WAKE_CTRL_5Hz 	0x40
#define BIT_LP_WAKE_CTRL_20Hz 	0x80
#define BIT_LP_WAKE_CTRL_40Hz 	0xC0
#define BIT_STBY_XA 			0x20
#define BIT_STBY_YA 			0x10
#define BIT_STBY_ZA 			0x08
#define BIT_STBY_XG 			0x04
#define BIT_STBY_YG 			0x02
#define BIT_STBY_ZG 			0x01

}  // namespace SensorRegMapName

#endif
