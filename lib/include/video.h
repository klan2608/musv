/*
 * video.h
 *
 *  Created on: May 26, 2015
 *      Author: yura
 */

#ifndef INCLUDE_VIDEO_H_
#define INCLUDE_VIDEO_H_

#include "gpio.h"
#include "osd.h"


namespace VideoName {

enum VideoMode_TypeDef
{
	VideoMode_TV,
	VideoMode_Photo
};


class Video {
	GpioName::Gpio* pinVideoSwitch;
	GpioName::Gpio* pinCameraPower;
	OsdName::Osd* osd;

public:
	Video(GpioName::GpioPort_TypeDef portSwitch, uint8_t pinSwitch,
			GpioName::GpioPort_TypeDef portCameraPower, uint8_t pinCameraPower,
			OsdName::Osd* osd);
	void Switch(VideoMode_TypeDef type);
};


}  // namespace VideoName



#endif /* INCLUDE_VIDEO_H_ */
