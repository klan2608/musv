/*
 * filter.cpp
 *
 *  Created on: 14 мая 2015 г.
 *      Author: klan
 */
#include "../lib/include/filter.h"

using namespace FilterName;
using namespace SensorName;
using namespace UsartName;
using namespace DelayName;
using namespace EepromName;


Filter::Filter() {
	MagneticDeclanation = 7.3f;
	angles.roll = 0.f;
	angles.pitch = 0.f;
	angles.yaw = 0.f;
}


void Filter::ReadDataFromMemory()
{
	float roll = 0.f, pitch = 0.f, yaw = 0.f;
	Eeprom::Read(eepromID, 0, &roll);
	Eeprom::Read(eepromID, 1, &pitch);
	Eeprom::Read(eepromID, 2, &yaw);

	bool correctData = true;
	correctData &= (roll > -180.f) & (roll < 180.f);
	correctData &= (pitch > -90.f) & (pitch < 90.f);
	correctData &= (yaw > -180.f) & (yaw < 180.f);

	if(correctData)
		SetAnglesBias(roll, pitch, yaw);
	else
		SetAnglesBias(0.f, 0.f, 0.f);
}

void Filter::WriteDataToMemory()
{
	Eeprom::Write(eepromID, 0, bias.roll);
	Eeprom::Write(eepromID, 1, bias.pitch);
	Eeprom::Write(eepromID, 2, bias.yaw);
}

void Filter::InitFilter(Sensor* accel, Axis<float>* gyro, Sensor* mag) {
	//TODO add check on GPS_VALID, else wait
	//Load Data

	FLOAT_TYPE UAV_Euler[3] = { 0.f, 0.f, 0.f };

	EKFdata.GravityAccel_mps2 = G_EARTH;
	EKFdata.MagDecl_deg = MagneticDeclanation; // Additional input needed to compute xdot, F, zhat, or H

	FLOAT_TYPE UAV_Acceleration[3] = { accel->GetValue()->X,
			accel->GetValue()->Y, accel->GetValue()->Z };

	FLOAT_TYPE Lambda[4] = { 0.f, 0.f, 0.f, 0.f };

	FLOAT_TYPE CurrentSpeed[3] = { 0.0, 0.0, 0.0 };

	FLOAT_TYPE InitPosX[3] = { 0.0, 0.0, 0.0 };

	FLOAT_TYPE InitBiasAccel[3] = { 0.0, 0.0, 0.0 };
	FLOAT_TYPE InitBiasGyro[3] = { gyro->X, gyro->Y, gyro->Z };

	//Roll
	UAV_Euler[1] = atan2(-UAV_Acceleration[1], -UAV_Acceleration[2]);
	//Pitch
	UAV_Euler[0] = -asin(-UAV_Acceleration[0] / G_EARTH);
	//Yaw
	UAV_Euler[2] = CurrentMagCourseCompensatedRad(accel, mag);

	//-----------------????????? ???????? ??????????? ???????????-------------------------
	Lambda[0] = cos(UAV_Euler[2] / 2) * cos(UAV_Euler[0] / 2)
			* cos(UAV_Euler[1] / 2)
			+ sin(UAV_Euler[2] / 2) * sin(UAV_Euler[0] / 2)
					* sin(UAV_Euler[1] / 2);
	Lambda[1] = sin(UAV_Euler[1] / 2) * cos(UAV_Euler[0] / 2)
			* cos(UAV_Euler[2] / 2)
			- cos(UAV_Euler[1] / 2) * sin(UAV_Euler[2] / 2)
					* sin(UAV_Euler[0] / 2);
	Lambda[2] = sin(UAV_Euler[0] / 2) * cos(UAV_Euler[1] / 2)
			* cos(UAV_Euler[2] / 2)
			+ cos(UAV_Euler[0] / 2) * sin(UAV_Euler[1] / 2)
					* sin(UAV_Euler[2] / 2);
	Lambda[3] = cos(UAV_Euler[1] / 2) * sin(UAV_Euler[2] / 2)
			* cos(UAV_Euler[0] / 2)
			- sin(UAV_Euler[1] / 2) * cos(UAV_Euler[2] / 2)
					* sin(UAV_Euler[0] / 2);

	//Initial DataFrame insertion
	//-----dev EKF-----
	EKFdata.FillInitSigmas();

	EKFdata.SetInitX(Lambda, InitPosX, CurrentSpeed);

	EKFdata.SetInitBias(InitBiasGyro, InitBiasAccel);

	EKFdata.FillPQ();
}

void Filter::Update(Sensor* accel, Sensor* gyro, Sensor* mag,
		FLOAT_TYPE secElapsed) {
	//------------------------------------------------------------------------
	EKFdata.SetNewGyroData(gyro->GetValue()->X, gyro->GetValue()->Y,
			gyro->GetValue()->Z);

	EKFdata.SetNewAccelData(accel->GetValue()->X, accel->GetValue()->Y,
			accel->GetValue()->Z);

	EKFdata.dt = secElapsed;

	if (mag->DataReady() == 0) {
		EKFdata.EmptyZR();
	} else {
		EKFdata.FillZMag(mag->GetValue()->X, mag->GetValue()->Y,
				mag->GetValue()->Z);
		EKFdata.FillZPos(0.0, 0.0, 0.0);
		EKFdata.FillZVelocity(0.0, 0.0, 0.0);

		EKFdata.FillR();
	}

	EKFdata.FilterThis();
	static FLOAT_TYPE Lambda[4];

//		perform_ekf ( EKFdata );

//		//------ ?????????????? ?????????? ??????? ???????????------------
//		float sum = 0.0;
//		for (uint8_t i=0; i<4; i++ )
//			sum += EKFdata.xhat[i]*EKFdata.xhat[i];
//		sum = sqrt ( sum );
//
//		for (uint8_t i=0; i<4; i++ )
//			EKFdata.xhat[i] /= sum;

	memcpy(Lambda, EKFdata.xhat, sizeof(FLOAT_TYPE) * 4);
	//----end dev EKF---

	//-------------------17------------------------
	//Roll
	anglesRaw.roll = Rad2Deg
			* atan2(2 * (Lambda[2] * Lambda[3] + Lambda[1] * Lambda[0]),
					1 - 2 * (Lambda[1] * Lambda[1] + Lambda[2] * Lambda[2]));
	angles.roll = anglesRaw.roll - bias.roll;
	CheckAngles(angles.roll, -180.f, 180.f);
	//Pitch
	anglesRaw.pitch = Rad2Deg
			* asin(-2 * (Lambda[1] * Lambda[3] - Lambda[0] * Lambda[2]));
	angles.pitch = anglesRaw.pitch - bias.pitch;
	CheckAngles(angles.pitch, -90.f, 90.f);
	//Yaw
	anglesRaw.yaw = Rad2Deg
			* atan2(2 * (Lambda[1] * Lambda[2] + Lambda[0] * Lambda[3]),
					1 - 2 * (Lambda[2] * Lambda[2] + Lambda[3] * Lambda[3]));
	angles.yaw = anglesRaw.yaw - bias.yaw;
	CheckAngles(angles.yaw, -180.f, 180.f);
}

void Filter::CheckAngles(FLOAT_TYPE& angle, FLOAT_TYPE min, FLOAT_TYPE max) {
	if (angle > max)
		angle -= max*2;
	else if (angle < min)
		angle -= min*2;
}

FLOAT_TYPE Filter::CurrentMagCourseCompensatedRad(Sensor* accel, Sensor* mag) {
	static FLOAT_TYPE UAV_Acceleration[3];
	static FLOAT_TYPE CalcVector;
	static FLOAT_TYPE MagValueScaled[3];
	static FLOAT_TYPE cosRoll;
	static FLOAT_TYPE sinRoll;
	static FLOAT_TYPE cosPitch;
	static FLOAT_TYPE sinPitch;
	static FLOAT_TYPE Xh;
	static FLOAT_TYPE Yh;
	static FLOAT_TYPE heading;

	UAV_Acceleration[0] = accel->GetValue()->X;
	UAV_Acceleration[1] = accel->GetValue()->Y;
	UAV_Acceleration[2] = accel->GetValue()->Z;

	CalcVector = sqrt(
			UAV_Acceleration[0] * UAV_Acceleration[0]
					+ UAV_Acceleration[1] * UAV_Acceleration[1]
					+ UAV_Acceleration[2] * UAV_Acceleration[2]);
	UAV_Acceleration[0] /= CalcVector;
	UAV_Acceleration[1] /= CalcVector;
	UAV_Acceleration[2] /= CalcVector;

	MagValueScaled[0] = mag->GetValue()->X;
	MagValueScaled[1] = mag->GetValue()->Y;
	MagValueScaled[2] = mag->GetValue()->Z;

	//Calc vars
	cosRoll = sqrt(fabs(1.0f - UAV_Acceleration[1] * UAV_Acceleration[1])); //cheat abs
	sinRoll = UAV_Acceleration[1];
	cosPitch = sqrt(fabs(1.0f - UAV_Acceleration[0] * UAV_Acceleration[0])); //cheat abs
	sinPitch = UAV_Acceleration[0];
	// The tilt compensation algorithm.
	Xh = MagValueScaled[0] * cosPitch + MagValueScaled[2] * sinPitch;
	Yh = MagValueScaled[0] * sinRoll * sinPitch + MagValueScaled[1] * cosRoll
			- MagValueScaled[2] * sinRoll * cosPitch;

	heading = -(atan2(Yh, Xh) - EKFdata.MagDecl_deg * Deg2Rad);

	if (heading > M_PI) {
		heading -= M_2_PI;
	}

	return heading;
}

void Filter::SetAnglesBias(float roll, float pitch, float yaw) {
	if (roll < 180.f && roll > -180.f)
		bias.roll = roll;
	if (pitch < 90.f && pitch > -90.f)
		bias.pitch = pitch;
	if (yaw < 180.f && yaw > -180.f)
		bias.yaw = yaw;
}
