/**
 * @file filter.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Май 2015 года
*/

#ifndef INCLUDE_FILTER_H_
#define INCLUDE_FILTER_H_


#include "../lib/EKF/consts.h"
#include "../lib/EKF/mekf.h"
#include "sensor.h"
#include "usart.h"
#include "delay.h"
#include "eeprom.h"
///@brief Определение пространственного положения относительно местной вертикали и направления на север
namespace FilterName {
///@brief Углы относительно местной вертикали и направления на север
struct Angles
{
	float roll;	///< Крен
	float pitch;	///< Тангаж
	float yaw;	///< Рысканье
};

///@brief Определение пространственного положения относительно местной вертикал и направления на север
class Filter
{
	FilterData EKFdata;		///< Расширенный фильтр Калмана
	Angles angles;			///< Углы с учетом нулевых смещений при установке
	Angles anglesRaw;		///< Углы без учета нулевых смещений при установке
	Angles bias;			///< Нулевые смещения при установке
	float MagneticDeclanation;	///< Магнитное склонение
	uint8_t eepromID;		///< Идентификационный номер для использования памяти Eeprom

public:
	Filter();
	void InitFilter(SensorName::Sensor* accel, SensorName::Axis<float>* gyro, SensorName::Sensor* mag);
	void Update(SensorName::Sensor* accel, SensorName::Sensor* gyro, SensorName::Sensor* mag, FLOAT_TYPE secElapsed);
	void ReadDataFromMemory();
	void WriteDataToMemory();
	FLOAT_TYPE CurrentMagCourseCompensatedRad(SensorName::Sensor* accel, SensorName::Sensor* mag);
	void SetAnglesBias(float roll, float pitch, float yaw);
	inline void ReserveMemoryEeprom() {eepromID = EepromName::Eeprom::ReserveMemory(3);}
	inline Angles* GetAngles() {return &angles;}
	inline Angles* GetAnglesRaw() {return &anglesRaw;}
	void CheckAngles(FLOAT_TYPE& angle, FLOAT_TYPE min, FLOAT_TYPE max);

};

}  // namespace FilterName



#endif /* INCLUDE_FILTER_H_ */
