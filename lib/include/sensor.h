/*
 * sensor.h
 *
 *  Created on: May 14, 2015
 *      Author: yura
 */

#ifndef INCLUDE_SENSOR_H_
#define INCLUDE_SENSOR_H_

#include "stm32f4xx.h"
#include "sensor_RegMap/HMC8553L.h"
#include "sensor_RegMap/MPU9150.h"

namespace SensorName {

template <class T>
struct Axis
{
	T X;
	T Y;
	T Z;
};

enum SensorAxis_TypeDef
{
	SensorAxis_X = 0,
	SensorAxis_Y = 1,
	SensorAxis_Z = 2
};

enum SensorAxisDirection_TypeDef
{
	SensorAxisDirection_Normal = 1,
	SensorAxisDirection_Inverted = -1,
};

class Sensor {
	Axis<float> value;
	Axis<short> raw;
	Axis<SensorAxisDirection_TypeDef> direction;
	Axis<float> koef;
	Axis<float> bias;
	Axis<float> scale;
	SensorAxis_TypeDef axis[3];
	uint8_t limit;
	bool dataReady;

public:
	Sensor(uint8_t limit, float koefX, float koefY, float koefZ);
	Sensor(uint8_t limit, float koef);
	~Sensor();
	void SetAxisValue(SensorAxis_TypeDef type, uint8_t valueH, uint8_t valueL);
	void ResetData();
	void InvertAxisDirection(SensorAxis_TypeDef type);

	void SetAxisDirection(SensorAxisDirection_TypeDef valueX,
			SensorAxisDirection_TypeDef valueY, SensorAxisDirection_TypeDef valueZ);
	void SetKoef(float valueX, float valueY, float valueZ);
	void SetKoef(float value);
	void SetBias(float valueX, float valueY, float valueZ);
	void SetScale(float valueX, float valueY, float valueZ);
	void RemapAxis(SensorAxis_TypeDef valueX,
			SensorAxis_TypeDef valueY, SensorAxis_TypeDef valueZ);

	inline bool DataReady() {return dataReady;}
	inline void SetDataReady(bool Ready) {dataReady = Ready;}
	inline Axis<float>* GetKoef() {return &koef;}
	inline Axis<float>* GetBias() {return &bias;}
	inline Axis<float>* GetScale() {return &scale;}
	inline uint8_t GetLimit() {return limit;}
	inline void SetLimit(uint8_t Lim) {limit = Lim;}
	inline Axis<float>* GetValue() {return &value;}
};

}  // namespace SensorName

#endif /* INCLUDE_SENSOR_H_ */
