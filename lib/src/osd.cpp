/*
 * osd.cpp
 *
 *  Created on: May 26, 2015
 *      Author: yura
 */

#include "../lib/include/osd.h"

using namespace OsdName;
using namespace GpioName;

Osd::Osd(GpioPort_TypeDef portSwitch, uint8_t pinSwitch,
			GpioPort_TypeDef portPower, uint8_t pinPower,
			GpioPort_TypeDef portCS, uint8_t pinCS)
{
	this->pinSwitch = new Gpio(portSwitch, pinSwitch, GpioMode_Normal);
	this->pinPower = new Gpio(portPower, pinPower, GpioMode_Normal);
	this->pinCS = new Gpio(portCS, pinCS, GpioMode_Inverted);
}


