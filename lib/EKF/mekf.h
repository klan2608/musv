#ifndef _MEKF_H
#define _MEKF_H

#include "macros.h"
#include "string.h"
#include "cmath"

// Types description for using inside EKF
#define MATRIX_EPS 1e-6
#define MATRIXSIZE 32

struct FilterData {
    struct sigmasType {
        FLOAT_TYPE mag3D_unitVector_meas;
        FLOAT_TYPE pos_meas_m;
        FLOAT_TYPE vel_meas_mps;
        FLOAT_TYPE gyro_bias_rps;
        FLOAT_TYPE accel_bias_mps2;
        FLOAT_TYPE quat_process_noise;
        FLOAT_TYPE pos_process_noise_m;
        FLOAT_TYPE vel_process_noise_mps;
        FLOAT_TYPE gyroBias_process_noise_rps;
        FLOAT_TYPE accelBias_process_noise_mps2;
        FLOAT_TYPE large_quat_uncertainty;
        FLOAT_TYPE large_pos_uncertainty_m;
        FLOAT_TYPE large_vel_uncertainty_mps;
    } sigmas;
    /* Final estemation results are here:
     * 0 - 3: quaternion
     * 4 - 6: position
     * 7 - 9: velocity
     * 10 - 12: gyro bias
     * 13 - 15: accel bias
     */
    FLOAT_TYPE xhat [16];
    FLOAT_TYPE F[MATRIXSIZE* MATRIXSIZE / 4];
    FLOAT_TYPE xdot[MATRIXSIZE / 2];
    FLOAT_TYPE H[9 * MATRIXSIZE / 2];
    FLOAT_TYPE zhat[9];
    //double P[16][16];
    FLOAT_TYPE P[256];
    //double Q[16][16];
    FLOAT_TYPE Q[256];
    // Parametrs
    FLOAT_TYPE gyro_rps[3];
    FLOAT_TYPE accel_mps2[3];
    FLOAT_TYPE GravityAccel_mps2;
    FLOAT_TYPE MagDecl_deg;
    FLOAT_TYPE z [9];
    //double R [9][9];
    FLOAT_TYPE R [81];
    //delta time
    FLOAT_TYPE dt;
    
    //Functions
    void SetInitX( FLOAT_TYPE InitQuat[4], FLOAT_TYPE InitPosition[3], FLOAT_TYPE InitVelocity[3] );
    void SetInitBias( FLOAT_TYPE InitBiasGyro[3], FLOAT_TYPE InitBiasAccel[3] );
    void FillInitSigmas();
    //Add sensor data
    void SetNewAccelData( FLOAT_TYPE Ax, FLOAT_TYPE Ay, FLOAT_TYPE Az );
    void SetNewGyroData( FLOAT_TYPE Gx, FLOAT_TYPE Gy, FLOAT_TYPE Gz );
    void FillZMag( FLOAT_TYPE Mx, FLOAT_TYPE My, FLOAT_TYPE Mz );
    void FillZPos( FLOAT_TYPE Nm, FLOAT_TYPE Em, FLOAT_TYPE Hm );
    void FillZVelocity( FLOAT_TYPE VN, FLOAT_TYPE VE, FLOAT_TYPE VH );
    //Fill matrixies
    void FillR();
    void FillPQ();
    void EmptyZR();
    //Init
    FilterData();
    void FilterThis();
    void compute_zhat_and_H__ins_ekf_quaternion();
    void compute_xdot_and_F__ins_ekf_quaternion();
};
#endif
