/*
 * sensor.cpp
 *
 *  Created on: May 14, 2015
 *      Author: yura
 */

#include "../lib/include/sensor.h"

using namespace SensorName;

Sensor::Sensor(uint8_t limit, float koefX, float koefY, float koefZ)
{
	this->limit = limit;
	this->koef.X = koefX;
	this->koef.Y = koefY;
	this->koef.Z = koefZ;

	this->scale.X = 1.f;
	this->scale.Y = 1.f;
	this->scale.Z = 1.f;

	this->bias.X = 0.f;
	this->bias.Y = 0.f;
	this->bias.Z = 0.f;

	this->direction.X = SensorAxisDirection_Normal;
	this->direction.Y = SensorAxisDirection_Normal;
	this->direction.Z = SensorAxisDirection_Normal;

	axis[SensorAxis_X] = SensorAxis_X;
	axis[SensorAxis_Y] = SensorAxis_Y;
	axis[SensorAxis_Z] = SensorAxis_Z;

	ResetData();
	dataReady = false;
}

Sensor::Sensor(uint8_t limit, float koef) : Sensor(limit, koef, koef, koef)
{
}


void Sensor::SetAxisValue(SensorAxis_TypeDef type, uint8_t valueH, uint8_t valueL)
{
	switch(axis[type])
	{
	case SensorAxis_X:
		raw.X = valueH<<8;
		raw.X |= valueL;
		value.X = (koef.X * direction.X * raw.X - bias.X) * scale.X;
		break;
	case SensorAxis_Y:
		raw.Y = valueH<<8;
		raw.Y |= valueL;
		value.Y = (koef.Y * direction.Y * raw.Y - bias.Y) * scale.Y;
		break;
	case SensorAxis_Z:
		raw.Z = valueH<<8;
		raw.Z |= valueL;
		value.Z = (koef.Z * direction.Z * raw.Z - bias.Z) * scale.Z;
		break;
	default:
		break;
	}
}

void Sensor::SetAxisDirection(SensorAxisDirection_TypeDef valueX,
		SensorAxisDirection_TypeDef valueY, SensorAxisDirection_TypeDef valueZ){
	direction.X = valueX;
	direction.Y = valueY;
	direction.Z = valueZ;
}

void Sensor::InvertAxisDirection(SensorAxis_TypeDef type)
{
	switch(type)
	{
	case SensorAxis_X:
		direction.X = (SensorAxisDirection_TypeDef) (direction.X * SensorAxisDirection_Inverted);
		break;
	case SensorAxis_Y:
		direction.Y = (SensorAxisDirection_TypeDef) (direction.X * SensorAxisDirection_Inverted);
		break;
	case SensorAxis_Z:
		direction.Z = (SensorAxisDirection_TypeDef) (direction.X * SensorAxisDirection_Inverted);
		break;
	default:
		break;
	}
}

void Sensor::RemapAxis(SensorAxis_TypeDef valueX,
			SensorAxis_TypeDef valueY, SensorAxis_TypeDef valueZ)
{
	axis[SensorAxis_X] = valueX;
	axis[SensorAxis_Y] = valueY;
	axis[SensorAxis_Z] = valueZ;
}


void Sensor::SetKoef(float valueX, float valueY, float valueZ)
{
	koef.X = valueX;
	koef.Y = valueY;
	koef.Z = valueZ;
}


void Sensor::SetBias(float valueX, float valueY, float valueZ)
{
	bias.X = valueX;
	bias.Y = valueY;
	bias.Z = valueZ;
}

void Sensor::SetScale(float valueX, float valueY, float valueZ)
{
	scale.X = valueX;
	scale.Y = valueY;
	scale.Z = valueZ;
}

void Sensor::SetKoef(float value)
{
	SetKoef(value, value, value);
}

void Sensor::ResetData()
{
	raw.X = 0;
	raw.Y = 0;
	raw.Z = 0;
	value.X = 0.f;
	value.Y = 0.f;
	value.Z = 0.f;
}
